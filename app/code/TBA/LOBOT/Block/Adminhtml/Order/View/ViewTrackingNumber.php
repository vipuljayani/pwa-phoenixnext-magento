<?php
namespace TBA\LOBOT\Block\Adminhtml\Order\View;

class ViewTrackingNumber extends \Magento\Framework\View\Element\Template
{
	public function __construct(\Magento\Framework\View\Element\Template\Context $context)
	{
		parent::__construct($context);
	}

	public function getTrackingNumber()
    {
    	$orderId = $this->getRequest()->getParam('order_id');
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderId);


        return $order->getData('trackingNumber');
    }

    public function getShippingDate()
    {
    	$orderId = $this->getRequest()->getParam('order_id');
    	$objectManager = \Magento\Framework\App\ObjectManager::getInstance();
		$order = $objectManager->create('Magento\Sales\Api\Data\OrderInterface')->load($orderId);


        return $order->getData('shippingDate');
    }
}