<?php
namespace TBA\LOBOT\Cron;
use \Psr\Log\LoggerInterface;
use \TBA\LOBOT\Controller\LobotHelper\LobotSftpHelper;
use \TBA\LOBOT\Controller\LobotHelper\LobotSetOrderTrackingNumber;

class ConfirmShipmentResultFromLobotOnSchedule
{
	protected $logger;
	protected $lobotSftpHelper;
	protected $lobotSetOrderTrackingNumber;

	public function __construct(
		LoggerInterface $logger,
		LobotSetOrderTrackingNumber $lobotSetOrderTrackingNumber,
		LobotSftpHelper $lobotSftpHelper,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Sales\Block\Order\History $orderHistory)
	{
	   $this->logger = $logger;
       $this->orderHistory = $orderHistory;
       
       $this->lobotSetOrderTrackingNumber = $lobotSetOrderTrackingNumber;
		$this->lobotSftpHelper = $lobotSftpHelper;
	}

	public function run()
  {
    $iFlist = $this->lobotSftpHelper->GetLobotShipmentResult();

    $productTargetInfo = array();
    $dir = '/var/www/html/tmpLobotInterface/';

    foreach ($iFlist as $filename) 
    {
      $myfile = fopen($dir . $filename, "r") or die("Unable to open file!");
      while(!feof($myfile)) {
        $content = fgets($myfile);
        if($content == "")
          continue;

        $bean = $this->ShippingResultDataParser($content);
        $targetOrder = trim($bean['orderId']," ");
        $dataBean  = array(
        'trackingNumber'=>trim($bean['trackingNumber']," "),
        'shippingDate'=>trim($bean['shippingDate']," "),
        );

        $productTargetInfo[$targetOrder] = $dataBean;
      }
      fclose($myfile);
    }

    $this->lobotSetOrderTrackingNumber->UpdateTrackingNumberByOrderId($productTargetInfo);
  }

  private function ShippingResultDataParser($content)
  {
    $orderId = substr($content,0,10);
    $trackingNumber = substr($content,126,20);
    $shippingDate = substr($content,110,8);
    $bean  = array(
        'orderId'=>$orderId,
        'trackingNumber'=>$trackingNumber,
        'shippingDate'=>$shippingDate,
    );

    return $bean;
  }
}