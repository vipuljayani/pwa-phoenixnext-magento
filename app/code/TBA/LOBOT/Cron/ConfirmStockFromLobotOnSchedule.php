<?php
namespace TBA\LOBOT\Cron;
use \Psr\Log\LoggerInterface;
use \TBA\LOBOT\Controller\LobotHelper\TbaComfirmStockHelper;

class ConfirmStockFromLobotOnSchedule
{
	protected $logger;
	protected $tbaComfirmStockHelper;

	public function __construct(
		LoggerInterface $logger,
		TbaComfirmStockHelper $tbaComfirmStockHelper,
       \Magento\Framework\App\Action\Context $context)
	{
	   $this->logger = $logger;
       $this->tbaComfirmStockHelper = $tbaComfirmStockHelper;
	}

	function run()
	{
		$this->tbaComfirmStockHelper->ConfirmStockFromLobot();
	}
}