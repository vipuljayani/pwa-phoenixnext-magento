<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace TBA\LOBOT\Controller\Page;
use \TBA\LOBOT\Controller\LobotHelper\LobotUpdateStockHelper;
use \Psr\Log\LoggerInterface;

class TbaUpdateProductItemCodeByBarcodeFromCsv extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    protected $lobotUpdateStockHelper;
    protected $logger;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(LoggerInterface $logger,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LobotUpdateStockHelper $lobotUpdateStockHelper)
    {
       $this->resultJsonFactory = $resultJsonFactory;

       parent::__construct($context);
       $this->logger = $logger;
       $this->lobotUpdateStockHelper = $lobotUpdateStockHelper;
    }
    /**
     * View  page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
      $result = $this->resultJsonFactory->create();
      $data = ['message' => 'Hello world!'];
      echo 'TbaUpdateProductItemCodeByBarcodeFromCsv';

      $dir = '/var/www/html/tmpLobotInterface/ProductItemCodeByBarcode.csv';

      $file = fopen($dir,"r");
      $productList = array();
      while(! feof($file))
      {
          $content = fgetcsv($file);
          $productList[$content[0]] = $content[1];
      }

      fclose($file);

      $this->lobotUpdateStockHelper->UpdateProductItemCodeByBarcode($productList);
      return $result->setData($data);
    }

    /*private function GetLobotUpdateStockLocalFile()
    {
      $filesNameList = array();
      $dir = '/home/admin/domains/phoenixnext.com/public_html/tmpLobotInterface/';
      if ($handle = opendir($dir)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
            }
        }
        closedir($handle);
      }

      $glob_key = $dir . 'BAEZSY*';

      $loggerMessage = "Finding files : " . $glob_key;
      $this->logger->info($loggerMessage);
      echo $loggerMessage . "\n";

      foreach (glob($glob_key) as $filename) 
      {
          $loggerMessage = "Found file " . basename($filename);
          $this->logger->info($loggerMessage);
          echo $loggerMessage . "\n";
          array_push($filesNameList,basename($filename));
      }

      return $filesNameList;
    }*/
}
