<?php
namespace TBA\LOBOT\Controller\Page;
use \Psr\Log\LoggerInterface;

class TbaResetOrderListToPending extends \Magento\Framework\App\Action\Action
{
    protected $resultJsonFactory;
    protected $orderFactory;
    protected $logger;

    public function __construct(LoggerInterface $logger,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
       \Magento\Sales\Model\OrderFactory $orderFactory)
    {
       $this->orderFactory = $orderFactory;

       parent::__construct($context);
       $this->logger = $logger;
       $this->resultJsonFactory = $resultJsonFactory;
    }

    public function execute()
    {
      $result = $this->resultJsonFactory->create();
      $data = ['message' => 'Hello world!'];
      echo 'TbaResetOrderListToPending Start' . "\r\n";

      $filename = '/var/www/html/tmpLobotInterface/OrderListCsvToRest.csv';

      $file = fopen($filename,"r");
      while(! feof($file))
      {
          $content = fgetcsv($file);
          $pad_length = 9;
          $pad_char = 0;
          $str_type = 'd'; // treats input as integer, and outputs as a (signed) decimal number
          $format = "%{$pad_char}{$pad_length}{$str_type}"; // or "%04d"
          $targetId = sprintf($format, $content[0]);

          $orderTar = $this->orderFactory->create()->loadByIncrementId($targetId);
          if ($orderTar->getId()){
            $orderTar->setData('trackingNumber',"");
            $orderTar->setData('shippingDate',"");
            $orderTar->setState("Pending_2C2P");
            $orderTar->setStatus("Pending_2C2P");
            $orderTar->save();

            echo 'TbaResetOrderListToPending found Order ID : ' . $orderTar->getIncrementId() . " | now status is :: " . $orderTar->getStatus() . "\r\n";
            echo "TbaResetOrderListToPending complete Order : " . $orderTar->getIncrementId() . " : with TN : " . $orderTar->getData('trackingNumber') . "\r\n";
            echo "TbaResetOrderListToPending complete Order : " . $orderTar->getIncrementId() . " : with SD : " . $orderTar->getData('shippingDate') . "\r\n";
            echo "====================================================================================". "\r\n";
          }
          else
          {
            echo 'TbaResetOrderListToPending not found $content[0] : ' . $targetId . "\r\n";
            echo "====================================================================================". "\r\n";
          }
      }

      fclose($file);

      return $result->setData($data);
    }
}