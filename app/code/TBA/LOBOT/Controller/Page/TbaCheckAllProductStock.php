<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace TBA\LOBOT\Controller\Page;
use \TBA\LOBOT\Controller\LobotHelper\TbaCheckStockHelper;

class TbaCheckAllProductStock extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    protected $tbaCheckStockHelper;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(TbaCheckStockHelper $tbaCheckStockHelper,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory)
{
       $this->resultJsonFactory = $resultJsonFactory;
       $this->tbaCheckStockHelper = $tbaCheckStockHelper;
       parent::__construct($context);
}
    /**
     * View  page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
      echo 'TbaCheckAllProductStock  execute';
       $result = $this->resultJsonFactory->create();
       $data = ['message' => 'Hello world!'];
       $this->tbaCheckStockHelper->CheckAllProductQty();

		  return $result->setData($data);
	   } 
}