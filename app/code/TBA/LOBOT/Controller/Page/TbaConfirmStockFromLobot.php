<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace TBA\LOBOT\Controller\Page;
use \TBA\LOBOT\Controller\LobotHelper\TbaComfirmStockHelper;
use \Psr\Log\LoggerInterface;

class TbaConfirmStockFromLobot extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;
    protected $tbaComfirmStockHelper;
    protected $logger;
    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     */
    public function __construct(LoggerInterface $logger,
       \Magento\Framework\App\Action\Context $context,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        TbaComfirmStockHelper $tbaComfirmStockHelper)
    {
       $this->resultJsonFactory = $resultJsonFactory;

       parent::__construct($context);
       $this->logger = $logger;
       $this->tbaComfirmStockHelper = $tbaComfirmStockHelper;
    }
    /**
     * View  page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
      $result = $this->resultJsonFactory->create();
      $data = ['message' => 'Hello world!'];
      echo 'TbaConfirmStockFromLobot';

      $this->tbaComfirmStockHelper->ConfirmStockFromLobot();
      return $result->setData($data);
    }
}