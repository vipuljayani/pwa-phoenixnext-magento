<?php /**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace TBA\LOBOT\Controller\LobotHelper;
use \TBA\LOBOT\Controller\LobotHelper\LobotUpdateStockHelper;
use \TBA\LOBOT\Controller\LobotHelper\LobotSftpHelper;
use \Psr\Log\LoggerInterface;

class TbaComfirmStockHelper
{

    protected $resultJsonFactory;
    protected $lobotUpdateStockHelper;
    protected $lobotSftpHelper;
    protected $logger;

    public function __construct(LoggerInterface $logger,
       \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        LobotUpdateStockHelper $lobotUpdateStockHelper,
        LobotSftpHelper $lobotSftpHelper)
    {
       $this->resultJsonFactory = $resultJsonFactory;

       $this->logger = $logger;
       $this->lobotUpdateStockHelper = $lobotUpdateStockHelper;
       $this->lobotSftpHelper = $lobotSftpHelper;
    }
    /**
     * View  page action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function ConfirmStockFromLobot()
    {
      $result = $this->resultJsonFactory->create();

      $dir = '/var/www/html/tmpLobotInterface/';
      $binDir = '/var/www/html/tmpLobotInterface/Bin/';

      $iFlist = $this->lobotSftpHelper->GetLobotUpdateStock();
      //$iFlist = $this->GetLobotUpdateStockLocalFile();
      
      $productTargetInfo = array();
      foreach ($iFlist as $filename) 
      {
        //$myfile = fopen("/tmpLobotInterface/" . $filename, "r") or die("Unable to open file!");
        $myfile = fopen($dir . $filename, "r") or die("Unable to open file!");

        // Output one line until end-of-file
        while(!feof($myfile)) {
          $content = fgets($myfile);
          $bean = $this->UpdateStockDataParser($content);
          $targetSku = trim($bean['HIN_COD']," ");
          $targetQty = (int)$bean['NUM_TYOBO_ZKO_SU'];
          $productTargetInfo[$targetSku] = $targetQty;
          //array_push($productTargetInfo, $this->UpdateStockDataParser($content));
        }

        fclose($myfile);

        rename($dir . $filename, $binDir . $filename);
      }

      $this->lobotUpdateStockHelper->CheckPreOrderWhenStockIn($productTargetInfo);
    }

    private function GetLobotUpdateStockLocalFile()
    {
      $filesNameList = array();
      $dir = '/var/www/html/tmpLobotInterface/';
      if ($handle = opendir($dir)) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
            }
        }
        closedir($handle);
      }

      $glob_key = $dir . 'BAEZSY*';

      $loggerMessage = "Finding files : " . $glob_key;
      $this->logger->info($loggerMessage);


      foreach (glob($glob_key) as $filename) 
      {
          $loggerMessage = "Found file " . basename($filename);
          $this->logger->info($loggerMessage);

          array_push($filesNameList,basename($filename));
      }

      return $filesNameList;
    }

    private function UpdateStockDataParser($content)
    {
      $REC = substr($content,0,1);
      $KEY_INF = substr($content,0,1);
      $DATA_KBN = substr($content, 1, 3);
      $TNO_JSI_YMD = substr($content, 4,8);
      $TNO_JSI_YM = substr($content, 12,6);
      $PLANT = substr($content, 18,4);
      $HOK_BSY = substr($content, 22,4);
      $SHN_TPY = substr($content, 26,4);
      $HIN_COD = substr($content, 29,18);
      $LOT_NO = substr($content, 48,15);
      $SYOUSAI_INF = substr($content, 63,1);
      $ZKO_KBN = substr($content, 63,1);
      $LOCK_KBN = substr($content, 64,1);
      $TYOBO_ZKO_SU = substr($content, 65,1);
      $NUM_TYOBO_ZKO_SU = substr($content, 64,13);
      $JCI_TNO_SU = substr($content, 78,1);
      $NUM_JCI_TNO_SU = substr($content, 78,13);
      $JCI_TNO_SU_TNI = substr($content, 91,3);
      $RIYU_COD = substr($content, 94,4);
      $TNO_FUD = substr($content, 98,10);
      $bean  = array(
          'REC'=>$REC,
          'KEY_INF'=>$KEY_INF,
          'DATA_KBN'=>$DATA_KBN,
          'TNO_JSI_YMD'=>$TNO_JSI_YMD,
          'TNO_JSI_YM'=>$TNO_JSI_YM,
          'PLANT'=>$PLANT,
          'HOK_BSY'=>$HOK_BSY,
          'SHN_TPY'=>$SHN_TPY,
          'HIN_COD'=>$HIN_COD,
          'LOT_NO'=>$LOT_NO,
          'SYOUSAI_INF'=>$SYOUSAI_INF,
          'ZKO_KBN'=>$ZKO_KBN,
          'LOCK_KBN'=>$LOCK_KBN,
          'TYOBO_ZKO_SU'=>$TYOBO_ZKO_SU,
          'NUM_TYOBO_ZKO_SU'=>$NUM_TYOBO_ZKO_SU,
          'JCI_TNO_SU'=>$JCI_TNO_SU,
          'NUM_JCI_TNO_SU'=>$NUM_JCI_TNO_SU,
          'JCI_TNO_SU_TNI'=>$JCI_TNO_SU_TNI,
          'RIYU_COD'=>$RIYU_COD,
          'TNO_FUD'=>$TNO_FUD);

      //$productTargetInfo = array($bean['HIN_COD'] => $bean['NUM_TYOBO_ZKO_SU']);
      //$productTargetInfo[$bean['HIN_COD']] = $bean['NUM_TYOBO_ZKO_SU'];

      return $bean;
    }
}
