<?php
namespace TBA\LOBOT\Controller\LobotHelper;
use \Psr\Log\LoggerInterface;

class TbaCheckStockHelper
{
    protected $objectManager;
    protected $logger;
    //protected $stockItemRepository;
    
    public function __construct(LoggerInterface $logger) {
        $this->logger = $logger;
        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        //$this->stockItemRepository = $this->objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');
    }

    public function UpdateProductAfterCheckOutBySkuQty($productList)
    {
        $_qtyProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addFieldToFilter('sku', array_keys($productList));
        $this->CheckProductFromList($_qtyProducts);
    }

    public function CheckAllProductQty()
    {
        $_qtyProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addAttributeToSelect('*');
        $this->CheckProductFromList($_qtyProducts);
    }

    private function CheckProductFromList($productList)
    {
        $_stockRegistry = $this->objectManager->get('\Magento\CatalogInventory\Api\StockRegistryInterface');
        if($productList) 
        {
            foreach ($productList as $_product) 
            {
                $status = "Ready";
                //$productId = $_product->getId();
                $_sku = $_product->getSku();
                $_stockItem = $_stockRegistry->getStockItemBySku($_sku);
                $stockQty = $_stockItem->getQty();
                //$stockQty = $stockState->getStockQty($_product->getId(), $_product->getStore()->getWebsiteId());
                //echo "<br>" . "==========================================";
                //echo "<br>" . "Get product id :: " . $productId;
                //echo "<br>" . "Get siteId :: " . $siteId;
                //echo "<br>" . "Get stockQty :: " . $stockQty;
                //if($stockQty < 20)
                //{
                    //$status = "PreOrder";
                //}
                if($_product->getData('stock_status') == "PreOrder")
                    continue;
                
                $_sku = $_product->getSku();
                $_product->setData('stock_status', $status)->getResource()->saveAttribute($_product, 'stock_status');

                $loggerMessage = "Finished product : " . $_sku . " QTY : " . $stockQty . " Status : " . $status;
                $this->logger->info($loggerMessage);
                //echo $loggerMessage;
                //echo "<br>" . "==========================================";
            }
        }
        else 
        {
            $this->logger->info("0 Products found with provided SKU's.");
        }
    }
}