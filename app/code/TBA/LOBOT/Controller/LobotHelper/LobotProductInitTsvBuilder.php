<?php

namespace TBA\LOBOT\Controller\LobotHelper;
use \Psr\Log\LoggerInterface;
//use \TBA\LOBOT\Controller\LobotHelper\LobotConstants;
use \Datetime;

class LobotProductInitTsvBuilder {
    protected $logger;
    //protected $lobotConstants;
    
    public function __construct(
        //LobotConstants $lobotConstants,
        LoggerInterface $logger) {
        $this->logger = $logger;
        //$this->lobotConstants = $lobotConstants;
    }

    public function GetOmissionName($name)
    {
        $half = (int) ( (strlen($name) / 2) ); // cast to int incase str length is odd
        return substr($name, 0, $half);
    }
    
    public function ProductToLobotTsvBuilder($productData)
    {
        $loggerMessage = 'ProductToLobotTsvBuilder  productData :: ' . count($productData);
        $this->logger->info($loggerMessage);
        $productResultData = array();
        if (empty($productData) || count($productData) == 0) return;
        foreach ($productData as $product)
        {
            //$status = $product->getStatus();
            //echo "<br>" . "status : " . $status;

            /*if($status == 2)
                continue;*/

            $catIds = $product->getCategoryIds();
            if (empty($catIds) || count($catIds) == 0) continue;
            $targetIndex = count($catIds) - 1;
            if($targetIndex < 0)
                $targetIndex = 0;
            $catTargetId = $catIds[$targetIndex];

            $_objectManager = \Magento\Framework\App\ObjectManager::getInstance();
            $category = $_objectManager->create('Magento\Catalog\Model\Category')
                                            ->load($catTargetId);

            $barcode = $product->getData('barcode');
            $loggerMessage = "ProductToLobotTsvBuilder process " . $product->getName() . " | " . $category->getName() . " | " . $catTargetId;
            $this->logger->info($loggerMessage);
            if($barcode == 'None') 
            {
                $barcode = '';
            }
            else
            {
                str_replace(' ','0',$barcode);
                $barcode = mb_strimwidth($product->getData('barcode'),0,13);
            }
                                         
            $productMode =  '0';
            $productCompanyCd =  '3001';
            $productSku =  $product->getSku();
            $productGroupCd =  '';
            $productName =  $product->getName();
            $productSubName =  $product->getName();
            $productOmissionName =  $this->GetOmissionName($product->getName());
            $productPartManagement =  '0';
            $productPrice =  '';
            $productPlant =  '';
            $productJan =  $barcode;
            $productCopyright =  '';
            $productProfit =  '';
            $productSuppileCode =  '';
            $productReleaseDate =  '';
            $productDept =  '';
            $productWidth =  '';
            $productHeight =  '';
            $productQty =  '';
            $productAgeLimit =  '';
            $productTargetCd =  '';
            $productTargetName =  '';
            $productAge =  '';
            $productToyComCd =  '';
            $productStNum =  '';
            $productCompanyCatCd =  $catTargetId;
            $productCompanyCatName =  $category->getName();
            $productCompanyTargetCd =  '';
            $productCompanyTargetName =  '';
            $productPieceInPole =  '';
            $productExp =  '0';

            $targetString = $productMode."\t".
            $productCompanyCd."\t".
            $productSku."\t".
            $productGroupCd."\t".
            $productName."\t".
            $productSubName."\t".
            $productOmissionName."\t".
            $productPartManagement."\t".
            $productPrice."\t".
            $productPlant."\t".
            $productJan."\t".
            $productCopyright."\t".
            $productProfit."\t".
            $productSuppileCode."\t".
            $productReleaseDate."\t".
            $productDept."\t".
            $productWidth."\t".
            $productHeight."\t".
            $productQty."\t".
            $productAgeLimit."\t".
            $productTargetCd."\t".
            $productTargetName."\t".
            $productAge."\t".
            $productToyComCd."\t".
            $productStNum."\t".
            $productCompanyCatCd."\t".
            $productCompanyCatName."\t".
            $productCompanyTargetCd."\t".
            $productCompanyTargetName."\t".
            $productPieceInPole."\t".
            $productExp;
            array_push($productResultData, $targetString);

        }
        
        return $this->BuildTsvContent($productResultData);
    }
    
    private function BuildTsvContent($productResultData)
    {
        $tsvContent = "";
        
        foreach ($productResultData as $targetString)
        {
            $tsvContent .= $targetString . "\r\n";
        }
        
        return $this->BuildTsvFile($tsvContent);
    }
    
    private function BuildTsvFile($tsvContent)
    {
        $dt = new DateTime();
        $timeInformat = $dt->format('YmdHis');
        //$timeInformat = DateTime::createFromFormat('YmdHis', 'now');
        $fileName = "BAESHN".$timeInformat.".tsv";
        //$filePath = $this->lobotConstants->LOCAL_TMP_INTERFACE_DIR . $fileName;

        $filePath = '/var/www/html/tmpLobotInterface/'.$fileName;

        if ( ! file_exists($filePath) ) {
            touch($filePath);
        }
        
        $stringContent = $tsvContent;

        $filePutContents = file_put_contents($filePath, $stringContent, FILE_APPEND);
        if ($filePutContents !== false) 
        {
            $loggerMessage = "File created (" . basename($filePath) . ")";
            $this->logger->info($loggerMessage);
        } 
        else 
        {
            $loggerMessage = "Cannot create file (" . $fileName . ")";
            $this->logger->info($loggerMessage);
        }

        return $filePath;
    }
}
