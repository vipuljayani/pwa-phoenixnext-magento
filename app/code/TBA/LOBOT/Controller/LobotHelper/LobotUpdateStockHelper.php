<?php
namespace TBA\LOBOT\Controller\LobotHelper;
use \Psr\Log\LoggerInterface;

class LobotUpdateStockHelper
{
    protected $bootstrap;
    protected $objectManager;
    protected $logger;

    protected $productRepository;
    protected $orderRepository;
    protected $lobotOrderTsvBuilder;
    protected $tbaCheckStockHelper;

    private $resource;
    private $tempStockInList;
    
    public function __construct(LoggerInterface $logger,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \TBA\LOBOT\Controller\LobotHelper\TbaCheckStockHelper $tbaCheckStockHelper,
        \TBA\LOBOT\Controller\LobotHelper\LobotOrderTsvBuilder $lobotOrderTsvBuilder) 
    {
        $this->logger = $logger;

        $this->objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $this->productRepository = $productRepository;
        $this->orderRepository = $orderRepository;
        $this->lobotOrderTsvBuilder = $lobotOrderTsvBuilder;
        $this->tbaCheckStockHelper = $tbaCheckStockHelper;
        $this->tempStockInList = array();

        $this->resource = $this->objectManager->get('Magento\Framework\App\ResourceConnection');
    }
    
    public function CheckPreOrderWhenStockIn($productStockInList)
    {
        if(!$productStockInList)
            return;
        
        $this->tempStockInList = $productStockInList;

        /*$writeConnection = $this->resource->getConnection('core_write');
        $readConnection = $this->resource->getConnection('core_read');

        $queryStatement = "SELECT * FROM tba_preorder_order WHERE status = 0 ORDER BY create_at ASC";
        $orderQueue = $readConnection->fetchAll($queryStatement);

        foreach ($orderQueue as $orderPreOrder) 
        {   
            $queryStatement = "SELECT * FROM tba_preorder_order_products WHERE pre_order_id = " . $orderPreOrder['pre_order_id'] . " AND remain_qty > 0";
            $productQueue = $readConnection->fetchAll($queryStatement);
            $orderId = $orderPreOrder['order_id'];

            if($orderPreOrder['is_delivery_on_ready'] != 0)
            {
                $this->CheckOrderSendTogether($productQueue,$orderId);
            }
            else
            {
                $this->CheckOrderSendSeparately($productQueue,$orderId);
            }
        }*/

        $this->UpdateProductStockBySkuQty($this->tempStockInList);
    }

    public function UpdateProductBarcodeBySku($productStockInList)
    {
        //echo "\n" . 'UpdateProductBarcodeBySku' . "\n";

        $_qtyProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addFieldToFilter('sku', array_keys($productStockInList));
        $stockItemRepository = $this->objectManager->get('\Magento\CatalogInventory\Model\Stock\StockItemRepository');

        if($_qtyProducts) 
        {
            foreach ($_qtyProducts as $_product) 
            {
                $_sku = $_product->getSku();
                $barcode = $productStockInList[$_sku];
                $productId = $_product->getId();

                //echo "<br>" . "==========================================";
                //echo "<br>" . "Get product id :: " . $productId;
                $_sku = $_product->getSku();
                str_replace(' ','',$barcode);
                $barcode = preg_replace('/[^\p{L}\p{N}\s]/u', '', $barcode);
                

                $_product->setData('barcode', $barcode)->getResource()->saveAttribute($_product, 'barcode');

                $loggerMessage = "<br>" . "Finished product : " . $_product->getName() . " Barcode : |" . $barcode . "|";
                $this->logger->info($loggerMessage);
                echo $loggerMessage;
                //echo "<br>" . "==========================================";
            }
        }
        else 
        {
            $this->logger->info("0 Products found with provided SKU's.");
        }
    }

    public function UpdateProductItemCodeByBarcode($productStockInList)
    {
        //echo "\n" . 'UpdateProductItemCodeByBarcode' . "\n";

        $_qtyProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addFieldToFilter('barcode', array_keys($productStockInList));

        if($_qtyProducts) 
        {
            foreach ($_qtyProducts as $_product) 
            {
                $barcode = $_product->getData('barcode');
                $targetItemCode = $productStockInList[$barcode];

                $productId = $_product->getId();

                //echo "<br>" . "==========================================";
                //echo "<br>" . "Get product id :: " . $productId;

                $targetItemCode = preg_replace('/[^\p{L}\p{N}\s]/u', '', $targetItemCode);
                str_replace(' ','',$targetItemCode);

                $_product->setSku($targetItemCode);
                $_product->setUrlKey($targetItemCode);
                $_product->save();

                $loggerMessage = "<br>" . "Finished product : " . $barcode . " ItemCode : |" . $_product->getSku() . "|";
                $this->logger->info($loggerMessage);
                //echo $loggerMessage;
                //echo "<br>" . "==========================================";
            }
        }
        else 
        {
            $this->logger->info("0 Products found with provided SKU's.");
        }
    }

    public function UpdateProductBarcodeByItemCode($productStockInList)
    {
        //echo "\n" . 'UpdateProductItemCodeByBarcode' . "\n";

        $_qtyProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addFieldToFilter('barcode', array_keys($productStockInList));

        if($_qtyProducts) 
        {
            foreach ($_qtyProducts as $_product) 
            {
                $sku = $_product->getSku();
                $targetBarcode = $productStockInList[$sku];

                $productId = $_product->getId();

                //echo "<br>" . "==========================================";
                //echo "<br>" . "Get product id :: " . $productId;

                str_replace(' ','',$targetItemCode);
                $targetItemCode = preg_replace('/[^\p{L}\p{N}\s]/u', '', $targetItemCode);
                
                $_product->setData('barcode', $targetBarcode)->getResource()->saveAttribute($_product, 'barcode');
                $_product->save();

                $loggerMessage = "<br>" . "Finished product : " . $sku . " Barcode : |" . $targetBarcode . "|";
                $this->logger->info($loggerMessage);
                //echo $loggerMessage;
                //echo "<br>" . "==========================================";
            }
        }
        else 
        {
            $this->logger->info("0 Products found with provided SKU's.");
        }
    }

    public function UpdateTaxClassToBookCategory()
    {
        $categories = [3,4,5];
        $_qtyProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addAttributeToSelect('*')->addCategoriesFilter(['in' => $categories])->load();

        if($_qtyProducts) 
        {
            foreach ($_qtyProducts as $_product) 
            {
                $_product->setTaxClassId(0); //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                $_product->save();

                //echo "<br>" . "==========================================";
                $loggerMessage = "<br>" . "Finished product : " . $_product->getName();
                $this->logger->info($loggerMessage);
                //echo $loggerMessage;
                //echo "<br>" . "==========================================";
            }
        }
        else 
        {
            $this->logger->info("0 Products found with provided SKU's.");
        }
    }

    private function UpdateProductStockBySkuQty($productList)
    {
        if(!$productList)
            return;

        $_qtyProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addFieldToFilter('sku', array_keys($productList));
        $_stockRegistry = $this->objectManager->get('\Magento\CatalogInventory\Api\StockRegistryInterface');
        //var_dump($_qtyProducts);

        if($_qtyProducts) {
            foreach ($_qtyProducts as $_product) {
                $_sku = $_product->getSku();
                //$productId = $_product->getId();
                $_stockItem = $_stockRegistry->getStockItemBySku($_sku);
                $targetQty = $productList[$_sku];
                $_stockItem->setData('qty', $targetQty);
                $_stockItem->save();
                $loggerMessage = "LobotUpdateStock Finished processing : " . $_sku . " QTY : " . $targetQty;
                $this->logger->info($loggerMessage);

            }

            //$this->UpdateAllOutOfStockProduct($productList);
            
        }
        else
        {
            $this->logger->info("0 Products found with provided SKU's.");
        }
    }

    private function UpdateAllOutOfStockProduct($productList)
    {
        if(!$productList)
            return;

        $_stockRegistry = $this->objectManager->get('\Magento\CatalogInventory\Api\StockRegistryInterface');
        // echo '<br>' . "======================QtyProducts======================" . '<br>';
        $allQtyProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addAttributeToSelect('*');
        
        
        $productStockInList = array_keys($productList);
        $allProductSku = array();

        $index = 0;
        foreach ($allQtyProducts as $tmpProduct) {
            $allProductSku[$index] = $tmpProduct->getSku();
            $index++;
        }

        // echo "allProductSku :: " . count($allProductSku) . '<br>';
        // echo "productStockInList :: " . count($productStockInList);

        // echo '<br>' . "================ConvertProductInListArray===============" . '<br>';
        $diffProducts = array_diff($allProductSku,$productStockInList);
        // echo "diffProducts :: " . count($diffProducts);
        //var_dump($diffProducts);

        // echo '<br>' . "====================diffProducts=====================" . '<br>';

        $ouOfStockProducts = $this->objectManager->create('Magento\Catalog\Model\ResourceModel\Product\Collection')->addFieldToFilter('sku', array_values($diffProducts));
        foreach ($ouOfStockProducts as $_product) 
        {
            $status = $_product->getData("stock_status");
            if($status == "PreOrder")
                continue;
            
            $_sku = $_product->getSku();
            $_stockItem = $_stockRegistry->getStockItemBySku($_sku);
            $targetQty = 0;
            $_stockItem->setData('qty', $targetQty);
            $_stockItem->save();
            $loggerMessage = "UpdateAllOutOfStockProduct : " . $_sku . " is Out Of Stock.";
            $this->logger->info($loggerMessage);
        }

        $this->tbaCheckStockHelper->CheckAllProductQty();
    }

    private function CheckOrderSendTogether($productQueue ,$orderId)
    {
        $productCount = count($productQueue);
        $tmpProductList = array();
        foreach ($productQueue as $product) 
        {
            $productSku = $product['product_sku'];
            $productRemainQty = $product['remain_qty'];
            if(array_key_exists($productSku, $this->tempStockInList))
            {
                if($this->tempStockInList[$productSku] > $productRemainQty)
                {
                    $tmpProductList[$productSku] = $productRemainQty;
                }
                else
                {
                    return;
                }
            }
            else
            {
                return;
            }
        }

        if(count($tmpProductList) == $productCount)
        {
            $this->ApplyPreOrder($tmpProductList,$orderId);
        }
    }

    private function CheckOrderSendSeparately($productQueue ,$orderId)
    {
        $productCount = count($productQueue);
        $tmpProductList = array();
        foreach ($productQueue as $product) 
        {
            $productSku = $product['product_sku'];
            $productRemainQty = $product['remain_qty'];
            if(array_key_exists($productSku, $this->tempStockInList))
            {
                if($this->tempStockInList[$productSku] > $productRemainQty)
                {
                    $tmpProductList[$productSku] = $productRemainQty;
                }
            }
        }

        if(count($tmpProductList) == $productCount)
        {
            $this->ApplyPreOrder($tmpProductList,$orderId);
        }
    }


    private function ApplyPreOrder($tempProductList ,$orderId)
    {
        $loggerMessage = 'Start ApplyPreOrder for LOBOT order is ' . $orderId . ' ---> Product count is :: ' . count($tempProductList);
        $this->logger->info($loggerMessage);

        $targetOrder = $this->orderRepository->get($orderId);

        $customerId = $targetOrder->getCustomerId();

        $shippingAddressObj = $targetOrder->getShippingAddress();
        $telephone = "None";
        $clientName = "None";
        $orderDeliveryPostCode = "None";
        $taxPrice = 0;

        $taxId = "Tax ID : 0105559143340";
        $kadokawaName = "KADOKAWA AMARIN COMPANY LIMITED";
        $kadokawaAdress1 = "378 Chaiyaphruk Road,";
        $kadokawaAdress2 = "Taling Chan, Bangkok 10170";
        $kadokawaAdress3 = "TEL. 02-434-0333";


        if($shippingAddressObj)
        {
            $shippingAddressArray = $shippingAddressObj->getData();
            $telephone = $shippingAddressArray['telephone'];
            $clientName = $shippingAddressArray['firstname'] . ' ' . $shippingAddressArray['lastname'];
            
            $company = isset($shippingAddressArray['company']) ? $shippingAddressArray['company'] : "";
            $address1 = ' ' . $shippingAddressArray['street'];
            $address2 = ' ' . $shippingAddressArray['city'] . ' ' . $shippingAddressArray['region'];
            $address3 = ' ' . $shippingAddressArray['postcode'];

            $address = $company . $address1 . $address2 . $address3;
            
            $orderDeliveryPostCode = $shippingAddressArray['postcode'];
        }

        $products = array();
        foreach ($tempProductList as $sku => $qty) 
        {
            $this->tempStockInList[$sku] -= $qty;
            $product = $this->productRepository->get($sku);

            $barcode = $product->getData('barcode');
            
            $loggerMessage = 'Try to get product id :: ' . $product->getId() . ' ---> Name is :: ' . $product->getName();
            $this->logger->info($loggerMessage);

            $loggerMessage = 'Product is ready, Tax is :: ' . $product->getTaxAmount();
            $this->logger->info($loggerMessage);

            //$taxPrice += $product->getTaxAmount();
            $productDataBean = array(
               "record_indicator" => 'D',
               "item_number" => $product->getSku(),
               "product_number" => $barcode,
               "catagories" => $product->getCategoryIds(),
               "product_name" => $product->getName(),
               "price" => $product->getData('price'),
               "quantity" => $product->getData('qty_ordered'),
            );
            array_push($products,$productDataBean);
        }

        if(count($products) > 0)
        {
            $reward_spend = $targetOrder->getRewardsSpentAmount();
              if(empty($reward_spend))
                $reward_spend = 0;
        
            $orderDataBean = array(
               'record_indicator' => 'H',
               "r3" => "3001",
               "unique_number" => $orderId,
               "order_number" => $orderId,
               "order_serial_number" => $targetOrder->getIncrementId(),
               "order_date" => date('Ymd',strtotime($targetOrder->getCreatedAt())),
               "order_member_number" => $customerId,
               "order_name" => $clientName,
               "order_order_address1" => $address1,
               "order_order_address2" => $address2,
               "order_order_address3" => $address3,
               "order_delivery_post_code" => $orderDeliveryPostCode,
               "order_phone_number" => $telephone,
               "destination_name" => $clientName,
               "delivery_address" => $address,
               "delivery_address1" => $address1,
               "delivery_address2" => $address2,
               "delivery_address3" => $address3,
               "destination_phone_number" => $telephone,
               "name_of_client" => $taxId,
               "name_of_client_local" => $kadokawaName,
               "client_address1" => $kadokawaAdress1,
               "client_address2" => $kadokawaAdress2,
               "client_address3" => $kadokawaAdress3,
               "client_address1_local" => $kadokawaAdress1,
               "client_address2_local" => $kadokawaAdress2,
               "client_address3_local" => $kadokawaAdress3,
               "requestor_phone_number" => $telephone,
               "shipping_amount" => $targetOrder->getShippingAmount(),
               "reward_spend" => $reward_spend,
               "discount_amount" => $targetOrder->getDiscountAmount(),
               "order_tax" => $taxPrice,
            );
            $orderBean = array(
                "header" => $orderDataBean,
                "detials" => $products
            );
            
            $tsvFilePath = $this->lobotOrderTsvBuilder->OrderToLobotTsvBuilder($orderBean);
            //$this->lobotSftpHelper->PassFileToLobot($tsvFilePath);
        }

        

        $loggerMessage = 'Saved order id :: ' . $orderId;
        $this->logger->info($loggerMessage);
        $loggerMessage = 'Saved order Status :: ' . $targetOrder->getStatus();
        $this->logger->info($loggerMessage);
    }
}