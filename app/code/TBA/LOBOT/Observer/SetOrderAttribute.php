<?php
namespace TBA\LOBOT\Observer;
use \Psr\Log\LoggerInterface;
 
class SetOrderAttribute implements \Magento\Framework\Event\ObserverInterface
{

    protected $logger;
    public function __construct(LoggerInterface $logger) 
    {
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var \Magento\Sales\Model\Order $order */
        $order = $observer->getData('order');
        $orderId = $order->getId();

        $this->logger->info('LOBOT SetOrderAttribute set trackingNumber to Order -> ' . $orderId);
        $order->setData('trackingNumber','Wait for shipment.');
        $order->save();
        $this->logger->info('LOBOT SetOrderAttribute set trackingNumber success :: ' . $order->getData('trackingNumber'));
        return $this;
    }
}