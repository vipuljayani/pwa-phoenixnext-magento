define(
    [
    'jquery',
    'https://t.2c2p.com/SecurePayment/api/my2c2p.1.6.9.min.js',
    //'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/api/my2c2p.1.6.9.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/gibberish-aes/1.0.0/gibberish-aes.min.js',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/place-order',
    'Magento_Checkout/js/action/select-payment-method',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/payment/additional-validators',
    'mage/url',
    'ko',
    'Magento_Ui/js/modal/confirm'
    ],
    function (
        $,
        My2,
        GibberishAES,
        quote,
        urlBuilder,
        storage,
        customerData,
        Component,
        placeOrderAction,
        selectPaymentMethodAction,
        customer,
        checkoutData,
        additionalValidators,
        url, ko,
        confirmation
        ) {
        'use strict';

        self.specializationArray = ko.observableArray();

        if(customer.isLoggedIn()){
            /*$.ajax({
                type: "POST",
                url: url.build('p2c2p/token/index'),
                data: {userId : customer.customerData.id },
                async : false,
                success: function (response) {
                    self.specializationArray(response.items);
                },
                error: function (response) {
                    self.specializationArray(response);
                }
            });*/
        }
        return Component.extend({
            defaults: {
                template: 'TBA_P123Payment/payment/P123Payment.phtml'
            },
            placeOrder: function (data, event) {

                if (event) {
                    event.preventDefault();
                }
                var self = this,
                placeOrder,
                emailValidationResult = customer.isLoggedIn(),
                loginFormSelector = 'form[data-role=email-with-possible-login]';
                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                if (emailValidationResult && this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);
                                placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

                                $.when(placeOrder).fail(function () {
                                    self.isPlaceOrderActionAllowed(true);
                                }).done(this.afterPlaceOrder.bind(this));
                                return true;
                }
                return false;
            },
            selectPaymentMethod: function () {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod(this.item.method);
                return true;
            },
            afterPlaceOrder: function () {
                 window.location.replace(url.build('p123payment/payment/request'));
            },
            getData: function() {
                var radioValue = $("input[name='agentCode']:checked").val();
                return {
                    'method': this.item.method,
                    'additional_data': {
                      'agentCode': radioValue,
                    }
                }
            }
        });
    }
);
