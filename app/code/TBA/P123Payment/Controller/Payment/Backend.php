<?php
/*
 * Created by 2C2P
 * Date 20 June 2017
 * This Response action method is responsible for handle the 2c2p payment gateway response.
 */

namespace TBA\P123Payment\Controller\Payment;

class Backend extends \P2c2p\P2c2pPayment\Controller\AbstractCheckoutApiAction
{
	public function execute()
	{
		$result = $_REQUEST["paymentResponse"]; 
		$response = $this->parse123Response($result);

		$xmlResponse=simplexml_load_string($response);
		$post = array();
		foreach ($xmlResponse as $index => $node)
	    {
	        $post[$index] = (String)$node;
			$loggerMessage = "2c2p Receiving [" . $index . "][".(String)$node."].";
 			file_put_contents('2c2p/'.$reponseTime.'.txt', $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
	    }
		
		$post = $this->processP2c2pSecureResponse($post);
		
		$hashHelper   = $this->getHashHelper();
		$configHelper = $this->getConfigSettings();
		$objCustomerData = $this->getCustomerSession();
		//$isValidHash  = $hashHelper->isValidHashValue($post,$configHelper['secretKey']);
		$secretKey = $configHelper['secretKey'];

		//Get Payment getway response to variable.
		$payment_status_code = $post['respCode'];
		$transaction_ref 	 = $post['tranRef'];
		$approval_code   	 = $post['approvalCode'];
		$payment_status  	 = $post['status'];
		$order_id 		 	 = $post['uniqueTransactionCode'];

		//Get the object of current order.
		$order = $this->getOrderDetailByOrderId($order_id);

		$isValidHash  = $hashHelper->isValidHashValue2($post,$secretKey);

		//Check whether hash value is valid or not If not valid then redirect to home page when hash value is wrong.
		if(!$isValidHash) {
			$order->setState(\Magento\Sales\Model\Order::STATUS_FRAUD);
			$order->setStatus(\Magento\Sales\Model\Order::STATUS_FRAUD);
			$order->save();
			//return;
		}

		$metaDataHelper = $this->getMetaDataHelper();
		$metaDataHelper->savePaymentGetawayResponse($post,$order->getCustomerId());

		//check payment status according to payment response.
		if(strcasecmp($payment_status_code, "000") == 0 || strcasecmp($payment_status_code, "00") == 0 ) {
			//IF payment status code is success
			//Set the complete status when payment is completed.
			//$order->setState(\Magento\Sales\Model\Order::STATE_PROCESSING);
			//$order->setStatus(\Magento\Sales\Model\Order::STATE_PROCESSING);
			//$order->save();

		} else if(strcasecmp($payment_status_code, "001") == 0) {
			//Set the Pending payment status when payment is pending. like 123 payment type.
			$order->setState("Pending_2C2P");
			$order->setStatus("Pending_2C2P");
			$order->save();
		}

	}
}
