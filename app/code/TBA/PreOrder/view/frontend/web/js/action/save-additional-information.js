/**
 * Customer store credit(balance) application
 */
/*global define,alert*/
define(
    [
        'ko',
        'jquery',
        'mage/storage'
    ],
    function (ko, $, storage) {
        'use strict';
        return function () {
            var deferred = $.Deferred();
            var isDeliveryOnReady = 1;

            if ($('#is-delivery-on-ready').length > 0) {
                if ($('#is-delivery-on-ready').attr('checked') == 'checked') {
                    isDeliveryOnReady = 1;
                } else {
                    isDeliveryOnReady = 0;
                }
            }
            
            var params = {
                'is_delivery_on_ready': isDeliveryOnReady
            };
            storage.post(
                'preOrderModule/index/saveCustomCheckoutData',
                JSON.stringify(params),
                false
            ).done(
                function (result) {

                }
            ).fail(
                function (result) {

                }
            ).always(
                function (result) {
                    deferred.resolve(result);
                }
            );
            return deferred;
        };
    }
);
