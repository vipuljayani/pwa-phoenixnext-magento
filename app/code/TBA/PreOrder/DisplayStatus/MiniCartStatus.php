<?php

namespace TBA\PreOrder\DisplayStatus;

use Magento\Quote\Model\Quote\Item;

class MiniCartStatus
{
    public function aroundGetItemData($subject, \Closure $proceed, Item $item)
    {
        $data = $proceed($item);

        $tempProduct = $item->getProduct();

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $productCol = $objectManager->create('Magento\Catalog\Model\Product');
        //$productCol->clear();
        $product = $productCol->load($tempProduct->getId());

        $label = $tempProduct->getData('stock_status');

        switch ($label) 
        {
            case 'Ready':
                $label = "มีสินค้า (พร้อมส่ง)";
                break;
            case 'PreOrder':
                $label = "<font color='red'>สินค้าพรี-ออเดอร์</font>";
                break;
            default :
                $label = "มีสินค้า (พร้อมส่ง)";
                break;
        }

        $atts = [
            "stock_status" => $label
        ];

        return array_merge($data, $atts);
    }
}