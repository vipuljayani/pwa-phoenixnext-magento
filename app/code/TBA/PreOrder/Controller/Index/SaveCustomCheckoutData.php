<?php

namespace TBA\PreOrder\Controller\Index;
use \Psr\Log\LoggerInterface;

class SaveCustomCheckoutData extends \Magento\Framework\App\Action\Action
{
    protected $_sidebar;
	
    protected $_resultJsonFactory;
	
    protected $_jsonHelper;

    protected $_dataObjectFactory;
   
    protected $_cartTotalRepositoryInterface;


    public function __construct(
        LoggerInterface $logger,
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\Framework\DataObjectFactory $dataObjectFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Quote\Api\CartTotalRepositoryInterface $cartTotalRepositoryInterface,
        \Magento\Checkout\Model\Sidebar $sidebar
    ) {
        parent::__construct($context);
        $this->_resultJsonFactory = $resultJsonFactory;
        $this->_jsonHelper = $jsonHelper;
        $this->_dataObjectFactory = $dataObjectFactory;
        $this->logger = $logger;
        $this->_sidebar = $sidebar;
        $this->_cartTotalRepositoryInterface = $cartTotalRepositoryInterface;
    }

    public function execute()
    {
        $additionalData = $this->_dataObjectFactory->create([
            'data' => $this->_jsonHelper->jsonDecode($this->getRequest()->getContent()),
        ]);
        $targetParam = $additionalData->getData('is_delivery_on_ready');
        $this->logger->info('TBA\PreOrder --> SaveCustomCheckoutData ---> targetParam : ' . $targetParam);
		$this->_objectManager->get('Magento\Checkout\Model\Session')->setData('is_delivery_on_ready', $targetParam);
    }
}