<?php
/**
 * Mageplaza
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Mageplaza.com license that is
 * available through the world-wide-web at this URL:
 * https://www.mageplaza.com/LICENSE.txt
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade this extension to newer
 * version in the future.
 *
 * @category    Mageplaza
 * @package     Mageplaza_FreeGifts
 * @copyright   Copyright (c) Mageplaza (https://www.mageplaza.com/)
 * @license     https://www.mageplaza.com/LICENSE.txt
 */

namespace Mageplaza\FreeGifts\Plugin\QuoteApi;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Model\Quote;
use Mageplaza\FreeGifts\Helper\Rule as HelperRule;

/**
 * Class AbstractCart
 * @package Mageplaza\FreeGifts\Plugin\QuoteApi
 */
abstract class AbstractCart
{
    /**
     * @var HelperRule
     */
    protected $_helperRule;

    /**
     * GuestCart constructor.
     *
     * @param HelperRule $helperRule
     */
    public function __construct(
        HelperRule $helperRule
    ) {
        $this->_helperRule = $helperRule;
    }

    /**
     * @param Quote $result
     *
     * @return Quote
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function addFreeGiftRule($result)
    {
        $extAttr = $result->getExtensionAttributes();

        if ($extAttr !== null && $this->_helperRule->getHelperData()->isEnabled()) {
            $rules = $this->_helperRule->setExtraData(false)->setQuote($result)->getAllValidRules();
            $extAttr->setMpFreeGifts($rules);
        }

        return $result;
    }
}
