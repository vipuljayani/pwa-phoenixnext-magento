<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Trackorder
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Trackorder\Api;

/**
 * Track Order Management interface.
 * @api
 */
interface TrackOrderManagementInterface
{
    /**
     * Check order's current status.
     *
     * @param \Aureatelabs\Trackorder\Api\TrackOrderInterface $trackOrder
     * @return \Aureatelabs\Trackorder\Api\TrackOrderResultInterface $trackOrderResult
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function track(\Aureatelabs\Trackorder\Api\TrackOrderInterface $trackOrder);
}
