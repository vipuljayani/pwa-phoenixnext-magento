<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Trackorder
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Trackorder\Api;

/**
 * Track Order interface.
 * @api
 */
interface TrackOrderInterface
{
    /**
     * Get status.
     *
     * @return string|null
     */
    public function getStatus();

    /**
     * Set status.
     *
     * @param string $status
     * @return string
     */
    public function setStatus($status);

    /**
     * Get cart id.
     *
     * @return string|null
     */
    public function getCartId();

    /**
     * Set cart id.
     *
     * @param string $cartId
     * @return string
     */
    public function setCartId($cartId);
}
