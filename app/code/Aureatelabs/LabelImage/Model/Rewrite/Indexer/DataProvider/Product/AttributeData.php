<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_LabelImage
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\LabelImage\Model\Rewrite\Indexer\DataProvider\Product;

use Divante\VsbridgeIndexerCatalog\Api\CatalogConfigurationInterface;
use Divante\VsbridgeIndexerCatalog\Api\SlugGeneratorInterface;
use Divante\VsbridgeIndexerCatalog\Model\ProductUrlPathGenerator;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product\AttributeDataProvider;
use Divante\VsbridgeIndexerCore\Indexer\DataFilter;
use Magento\Store\Model\StoreManagerInterface;
use Divante\VsbridgeIndexerCatalog\Model\ResourceModel\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Divante\VsbridgeIndexerCatalog\Model\Attributes\ProductAttributes;
use Mirasvit\CatalogLabel\Model\Placeholder;
use Mirasvit\CatalogLabel\Helper\Data;
use Mirasvit\CatalogLabel\Block\Product\Label;
use Magento\Catalog\Model\ProductFactory;
use Mirasvit\CatalogLabel\Model\PlaceholderFactory;
use Mirasvit\CatalogLabel\Model\ResourceModel\Label\CollectionFactory;
use Magento\Customer\Model\Session;
use Magento\Catalog\Model\ProductRepository;
use Mirasvit\RewardsCatalog\Helper\Earn;
/**
 * Class AttributeData
 * @package Aureatelabs\LabelImage\Model\Rewrite\Indexer\DataProvider\Product
 */
class AttributeData extends \Divante\VsbridgeIndexerCatalog\Model\Indexer\DataProvider\Product\AttributeData
{
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var Product
     */
    private $product;


    /**
     * AttributeData constructor.
     *
     * @param CatalogConfigurationInterface $configSettings
     * @param SlugGeneratorInterface $slugGenerator
     * @param ProductUrlPathGenerator $productUrlPathGenerator
     * @param DataFilter $dataFilter
     * @param AttributeDataProvider $resourceModel
     * @param StoreManagerInterface $storeManager
     * @param Product $product
     * @param Configurable $configurable
     * @param Placeholder $placeholder,
     * @param Data $dataHelper,
     * @param Label $label,
     * @param ProductFactory $_productloader,
     * @param PlaceholderFactory $placeholderFactory,
     * @param CollectionFactory $labelCollectionFactory,
     * @param Session $customerSession
     * @param ProductRepository $productRepository
     * @param Earn $earnHelper
     */
    public function __construct(
        ProductAttributes $productAttributes,
        CatalogConfigurationInterface $configSettings,
        SlugGeneratorInterface $slugGenerator,
        ProductUrlPathGenerator $productUrlPathGenerator,
        DataFilter $dataFilter,
        AttributeDataProvider $resourceModel,
        StoreManagerInterface $storeManager,
        Product $product,
        Configurable $configurable,
        Placeholder $placeholder,
        Data $dataHelper,
        Label $label,
        ProductFactory $_productloader,
        PlaceholderFactory $placeholderFactory,
        CollectionFactory $labelCollectionFactory,
        Session $customerSession,
        ProductRepository $productRepository,
        Earn $earnHelper
    ) {

        $this->storeManager = $storeManager;
        $this->product = $product;
        $this->configurable = $configurable;
        $this->placeholder = $placeholder;
        $this->dataHelper = $dataHelper;
        $this->label = $label;
        $this->_productloader = $_productloader;
        $this->placeholderFactory = $placeholderFactory;
        $this->labelCollectionFactory = $labelCollectionFactory;
        $this->customerSession        = $customerSession;
        $this->_productRepository = $productRepository;
        $this->earnHelper = $earnHelper;
        parent::__construct(
            $productAttributes,
            $configSettings,
            $slugGenerator,
            $productUrlPathGenerator,
            $dataFilter,
            $resourceModel
        );
    }

    /**
     * @param array $indexData
     * @param int   $storeId
     *
     * @return array
     * @throws \Exception
     */
    public function addData(array $indexData, $storeId)
    {
        $indexData = parent::addData($indexData, $storeId);

        $configurable = [];
        foreach ($indexData as $idx => $product) {
            if ($product['type_id'] == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                $configurable[$product['id']] = $product['sku'];
            }
        }

        foreach ($indexData as $idx => $product) {
            $_product = $this->_productloader->create()->load($product['id']);
            // $html = $this->label->getDisplays($_product);
            //     var_dump($html);
            //     die();

            $result = [];
            $labels = [];
            $html = [];
            $veiwPlaceholders   = $this->placeholder->getCollection()
            ->addFieldTofilter('is_active', 1);

            foreach ($veiwPlaceholders as $p) {
                if ($this->label->getLabel()) {
                    $labels[] = $this->label->getLabel();
                } else {
                    $labels      = $this->labelCollectionFactory->create()
                        ->addActiveFilter()
                        ->addCustomerGroupFilter($this->customerSession->getCustomerGroupId())
                        ->addStoreFilter($this->storeManager->getStore())
                        ->addFieldToFilter('placeholder_id', $p->getId());
    
                    //it will be applied only if more than one labels are in the same position
                    $labels->getSelect()->order('sort_order ASC');
                }
    
                /** @var \Mirasvit\CatalogLabel\Model\Label $label */
                foreach ($labels as $label) {
                    $result = array_merge($result, $label->getDisplays($_product));
                }
    
                foreach ($result as $display) {
                    $display->setType($this->label->getType());
                    $html[] = $display->getData();
                }
               
            }
           $media_url = $this->storeManager->getStore()->getBaseUrl(\Magento\Framework\UrlInterface::URL_TYPE_MEDIA) . 'cataloglabel';
            if (!empty($html)) {
                foreach($html as $idy => $valueid){
                    $indexData[$idx]['product_sticker_img'][$idy] ='';
                    $indexData[$idx]['label_image'][$idy] = $media_url.$valueid['list_image'];
                }  
            }

            $customer = $this->customerSession->getCustomer();
            $websiteId = $this->storeManager->getDefaultStoreView()->getWebsiteId();
            $points = $this->earnHelper->getProductPoints($_product, $customer, $websiteId);
            $indexData[$idx]['product_reward_point'] = $points;


        }
        if ($product['type_id'] != \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {

            $parentId = $this->getParentProductId($product['id']);
            $indexData[$idx]['parent_id'] = (!empty($parentId)) ? $parentId : '';
            $indexData[$idx]['parent_sku'] = (!empty($parentId) && isset($configurable[$parentId])) ? $configurable[$parentId] : '';
        }
        
        return $indexData;
    }

    /**
     * @param int $childId
     * @return int
     */
    public function getParentProductId($childId)
    {
        $parentConfigObject = $this->configurable->getParentIdsByChild($childId);
        if($parentConfigObject) {
            return $parentConfigObject[0];
        }
        return '';
    }
}
