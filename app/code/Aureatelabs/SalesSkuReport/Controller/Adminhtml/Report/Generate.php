<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

 namespace Aureatelabs\SalesSkuReport\Controller\Adminhtml\Report;

 class Generate extends \Magento\Backend\App\Action {

    protected $resultJsonFactory;

    protected $resourceConnection;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\ResourceConnection $resourceConnection
    )
    {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->resourceConnection = $resourceConnection;
        parent::__construct($context);
    }
        

    public function execute()
    {
        $resultJson = $this->resultJsonFactory->create();
        $customerGender = $this->getRequest()->getParam("customerGender");
        $customerAge = $this->getRequest()->getParam("customerAge");
        $formDate = $this->getRequest()->getParam("formDate");
        $toDate = $this->getRequest()->getParam("toDate");

        $startDate = date("Y-m-d h:i:s",strtotime($formDate));
        $endDate = date("Y-m-d h:i:s", strtotime($toDate));

        $currentDate = date("Y-m-d");
        $aboveDate = strtotime($currentDate . ' ' . '-' . $customerAge .' year');
        $date = date("Y", $aboveDate);

        $query = "SELECT `sku`,COUNT(*) as qty,customer_gender as gender FROM `aureate_sku_sales`  WHERE (`customer_gender` = '" . $customerGender ."') AND (`order_date` >= '". $startDate ."') AND (`order_date` <= '". $endDate ."') AND (year(`customer_dob`) = '".$date."') GROUP BY sku HAVING COUNT(*) > 1 ORDER BY COUNT(*) DESC LIMIT 20";
        $results = $this->resourceConnection->getConnection()->fetchAll($query);

        return $resultJson->setData($results);
    }
}