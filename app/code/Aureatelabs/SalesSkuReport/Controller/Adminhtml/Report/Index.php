<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

 namespace Aureatelabs\SalesSkuReport\Controller\Adminhtml\Report;

 class Index extends \Magento\Backend\App\Action {
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    private $resultPageFactory;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    /**
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->set('SKU Sales Report');
        $resultPage->getConfig()->getTitle()->prepend(__('SKU Sales Report'));
        $resultPage->setActiveMenu('Magento_Reports::report_salesroot_skusales');
        $this->_addContent($this->_view->getLayout()->createBlock('Aureatelabs\SalesSkuReport\Block\Adminhtml\Grid\Grid'));
        $this->_view->renderLayout();
    }

    protected function _isAllowed() {
        return true;
    }
}