<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\SalesSkuReport\Model;

use Aureatelabs\SalesSkuReport\Api\Data\SkusalesInterface;

class Skusales extends \Magento\Framework\Model\AbstractModel implements SkusalesInterface {

    /**
     * Cache tag.
     */
     const CACHE_TAG = 'aureate_sku_sales';

     /**
      * @var string
      */
     protected $_cacheTag = 'aureate_sku_sales';
 
     /**
      * Prefix of model events names.
      *
      * @var string
      */
     protected $_eventPrefix = 'aureate_sku_sales';
 
     /**
      * Initialize resource model.
      */
     protected function _construct()
     {
         $this->_init('Aureatelabs\SalesSkuReport\Model\ResourceModel\Skusales');
     }

     public function getEntityId() {
        return $this->getData(self::ENTITY_ID);
     }

     public function setEntityId($entityId) {
        return $this->setData(self::ENTITY_ID, $entityId);
     }
 
     public function getOrderId() {
        return $this->getData(self::ORDER_ID);
     }
 
     public function setOrderId($orderId) {
        return $this->setData(self::ORDER_ID, $orderId);
     }
 
     public function getOrderDate() {
        return $this->getData(self::ORDER_DATE);
     }
 
     public function setOrderDate($orderDate) {
        return $this->setData(self::ORDER_DATE, $orderDate);
     }
 
     public function getCustomerId() {
        return $this->getData(self::CUSTOMER_ID);
     }
 
     public function setCustomerId($customerId) {
        return $this->setData(self::CUSTOMER_ID, $customerId);
     }
 
     public function getCustomerDob() {
        return $this->getData(self::CUSTOMER_DOB);
     }
 
     public function setCustomerDob($customerDob) {
        return $this->setData(self::CUSTOMER_DOB, $customerDob);
     }
 
     public function getCustomerGender() {
        return $this->getData(self::CUSTOMER_GENDER);
     }
 
     public function setCustomerGender($customerGender) {
        return $this->setData(self::CUSTOMER_GENDER, $customerGender);
     }

     public function getSku() {
        return $this->getData(self::SKU);
     }
 
     public function setSku($sku) {
        return $this->setData(self::SKU, $sku);
     }
 
     public function getCreatedAt() {
        return $this->getData(self::CREATED_AT);
     }
 
     public function setCreatedAt($createdAt) {
        return $this->setData(self::CREATED_AT, $createdAt);
     }
 
     public function getUpdatedAt() {
        return $this->getData(self::UPDATED_AT);
     }
 
     public function setUpdatedAt($updatedAt) {
        return $this->setData(self::UPDATED_AT, $updatedAt);
     }

 }