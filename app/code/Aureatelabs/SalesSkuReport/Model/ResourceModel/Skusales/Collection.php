<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

 namespace Aureatelabs\SalesSkuReport\Model\ResourceModel\Skusales;

 class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection {

    protected $_idFieldName = 'entity_id';

    /**
    * Define resource model.
    */
    protected function _construct()
    {
        $this->_init(
            'Aureatelabs\SalesSkuReport\Model\Skusales',
            'Aureatelabs\SalesSkuReport\Model\ResourceModel\Skusales'
        );
    }
 }