<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\SalesSkuReport\Model\Config\Source;

class Month implements \Magento\Framework\Data\OptionSourceInterface {

    public function toOptionArray()
    {
        return [
            ['value' => '0', 'label' => __('Select Month')],
            ['value' => '1', 'label' => __('Jan')],
            ['value' => '2', 'label' => __('Feb')],
            ['value' => '3', 'label' => __('Mar')],
            ['value' => '4', 'label' => __('Apr')],
            ['value' => '5', 'label' => __('May')],
            ['value' => '6', 'label' => __('Jun')],
            ['value' => '7', 'label' => __('Jul')],
            ['value' => '8', 'label' => __('Aug')],
            ['value' => '9', 'label' => __('Sept')],
            ['value' => '10', 'label' => __('Oct')],
            ['value' => '11', 'label' => __('Nov')],
            ['value' => '12', 'label' => __('Dec')]
        ];
    }

}
