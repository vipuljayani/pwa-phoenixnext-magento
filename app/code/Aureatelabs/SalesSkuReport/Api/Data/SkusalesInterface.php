<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_SalesSkuReport
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\SalesSkuReport\Api\Data;

interface SkusalesInterface {

    const ENTITY_ID = 'entity_id';
    const ORDER_ID = 'order_id';
    const ORDER_DATE = 'order_date';
    const CUSTOMER_ID = 'customer_id';
    const CUSTOMER_DOB = 'customer_dob';
    const CUSTOMER_GENDER = 'customer_gender';
    const SKU = 'sku';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function getEntityId();

    public function setEntityId($entityId);

    public function getOrderId();

    public function setOrderId($orderId);

    public function getOrderDate();

    public function setOrderDate($orderDate);

    public function getCustomerId();

    public function setCustomerId($customerId);

    public function getCustomerDob();

    public function setCustomerDob($customerDob);

    public function getCustomerGender();

    public function setCustomerGender($customerGender);

    public function getSku();

    public function setSku($sku);

    public function getCreatedAt();

    public function setCreatedAt($createdAt);

    public function getUpdatedAt();

    public function setUpdatedAt($updatedAt);

}