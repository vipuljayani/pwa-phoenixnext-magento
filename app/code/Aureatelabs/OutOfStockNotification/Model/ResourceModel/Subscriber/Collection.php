<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_OutOfStockNotification
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\OutOfStockNotification\Model\ResourceModel\Subscriber;

/**
 * Class Collection
 */

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'subscribe_id';

    /**
     * Define resource model.
     */
    protected function _construct()
    {
        $this->_init(
            'Aureatelabs\OutOfStockNotification\Model\Subscriber',
            'Aureatelabs\OutOfStockNotification\Model\ResourceModel\Subscriber'
        );
    }
}
