<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_OutOfStockNotification
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\OutOfStockNotification\Model;

use Aureatelabs\OutOfStockNotification\Api\Data\SubscriberInterface;

class Subscriber extends \Magento\Framework\Model\AbstractModel implements SubscriberInterface {

    /**
     * Cache tag.
     */
    const CACHE_TAG = 'aureatelabs_outofstocknotification_subscriber_subscriber';

    /**
     * @var string
     */
    protected $_cacheTag = 'aureatelabs_outofstocknotification_subscriber_subscriber';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'aureatelabs_outofstocknotification_subscriber_subscriber';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('Aureatelabs\OutOfStockNotification\Model\ResourceModel\Subscriber');
    }

    /**
     * Get Id.
     */
    public function getSubscribeId()
    {
        return $this->getData(self::SUBSCRIBE_ID);
    }

    /**
     * Set Id
     */
    public function setSubscribeId($subscribeId)
    {
        return $this->setData(self::SUBSCRIBE_ID, $subscribeId);
    }

    /**
     * Get product id.
     */
    public function getProductId()
    {
        return $this->getData(self::PRODUCT_ID);
    }

    /**
     * Set product id
     */
    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * Get customer email.
     */
    public function getCustomerEmail()
    {
        return $this->getData(self::CUSTOMER_EMAIL);
    }

    /**
     * Set customer email
     */
    public function setCustomerEmail($customerEmail)
    {
        return $this->setData(self::CUSTOMER_EMAIL, $customerEmail);
    }

    /**
     * Get is sent.
     */
    public function getIsSent()
    {
        return $this->getData(self::IS_SENT);
    }

    /**
     * Set is sent
     */
    public function setIsSent($isSent)
    {
        return $this->setData(self::IS_SENT, $isSent);
    }

     /**
     * Get Creation Time.
     *
     * @return \Aureatelabs\OutOfStockNotification\Api\Data\varchar|mixed
     */
    public function getCreatedAt()
    {
        return $this->getData(self::CREATED_AT);
    }

    /**
     * Set Creation Time.
     *
     * @param $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get Modification Time.
     *
     * @return \Aureatelabs\OutOfStockNotification\Api\Data\varchar|mixed
     */
    public function getUpdatedAt()
    {
        return $this->getData(self::UPDATE_AT);
    }

    /**
     * Set Modification Time.
     *
     * @param $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATE_AT, $updatedAt);
    }

}