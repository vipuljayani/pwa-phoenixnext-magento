<?php

namespace Aureatelabs\OutOfStockNotification\Cron;

use Aureatelabs\OutOfStockNotification\Model\ResourceModel\Subscriber\CollectionFactory;
use Magento\CatalogInventory\Model\Stock\StockItemRepository;
use Aureatelabs\OutOfStockNotification\Model\SubscriberFactory;
use Aureatelabs\OutOfStockNotification\Helper\Data;
use Magento\Catalog\Model\ProductFactory;

class CheckInStock {

    protected $subscriberFactory;

    protected $collectionFactory;

    protected $stockItemRepository;

    protected $product;

    protected $helper;

    public function __construct(
        CollectionFactory $collectionFactory,
        StockItemRepository $stockItemRepository,
        SubscriberFactory $subscriberFactory,
        Data $helper,
        ProductFactory $product
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->stockItemRepository = $stockItemRepository;
        $this->subscriberFactory = $subscriberFactory;
        $this->helper = $helper;
        $this->product = $product;
    }

    public function execute() {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('is_sent','0');

        foreach($collection as $item) {
            $productId = $item->getProductId();
            $stockData = $this->stockItemRepository->get($productId);
            if($stockData->getIsInStock()) {
                $product = $this->product->create()->load($productId);
                $productUrl = $product->getProductUrl();
                $receiver = $item->getCustomerEmail();
                $productName = $product->getName();
                $this->helper->sendEmail($productUrl, $productName, $receiver);
                $item->setIsSent(1);
                $item->save();
            }
        }
    }

}