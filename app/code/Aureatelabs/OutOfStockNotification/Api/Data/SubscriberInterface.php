<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_OutOfStockNotification
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\OutOfStockNotification\Api\Data;

interface SubscriberInterface {

    const SUBSCRIBE_ID = 'subscribe_id';
    const PRODUCT_ID = 'product_id';
    const CUSTOMER_EMAIL = 'customer_email'; 
    const IS_SENT = 'is_sent';
    const CREATED_AT = 'created_at';
    const UPDATE_AT = 'updated_at';

    public function getSubscribeId();

    public function setSubscribeId($subscribeId);

    public function getProductId();

    public function setProductId($productId);

    public function getCustomerEmail();

    public function setCustomerEmail($customerEmail);

    public function getIsSent();

    public function setIsSent($isSent);

    public function getCreatedAt();

    public function setCreatedAt($createdAt);

    public function getUpdatedAt();

    public function setUpdatedAt($updatedAt);

}