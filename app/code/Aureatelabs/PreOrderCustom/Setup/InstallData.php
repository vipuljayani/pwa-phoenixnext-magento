<?php

namespace Aureatelabs\PreOrderCustom\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Setup\SalesSetupFactory;
use Magento\Framework\DB\Ddl\Table;
use Magento\Quote\Setup\QuoteSetupFactory;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    /**
     * EAV setup factory
     *
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var QuoteSetupFactory
     */
    private $quoteSetupFactory;

    /**
     * @var SalesSetup
     */
    private $salesSetupFactory;

    /**
     * Init
     *
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(EavSetupFactory $eavSetupFactory, QuoteSetupFactory $quoteSetupFactory, SalesSetupFactory $salesSetupFactory)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->quoteSetupFactory = $quoteSetupFactory;
        $this->salesSetupFactory = $salesSetupFactory;
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
         /** @var QuoteSetup $quoteSetup */
         $quoteSetup = $this->quoteSetupFactory->create(['setup' => $setup]);
         /** @var SalesSetup $salesSetup */
         $salesSetup = $this->salesSetupFactory->create(['setup' => $setup]);
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        if (version_compare($context->getVersion(), '1.0.0') < 0) {

            $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'pre_order_qty');
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'pre_order_qty',
                [
                    'type' => 'varchar',
                    'label' => 'Pre Order Qty',
                    'input' => 'text',
                    'source' => '',
                    'frontend' => '',
                    'required' => false,
                    'backend' => '',
                    'sort_order' => '30',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'default' => null,
                    'visible' => true,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => true,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'unique' => false,
                    'apply_to' => 'simple,grouped,bundle,configurable,virtual',
                    'group' => 'Attributes',
                    'used_in_product_listing' => false,
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => true,
                    'is_html_allowed_on_front' => true
                ]
            );

            $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'pre_order_from');
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'pre_order_from',
                [
                    'type' => 'datetime',
                    'label' => 'Pre Order From',
                    'input' => 'date',
                    'source' => '',
                    'frontend' => '',
                    'required' => false,
                    'backend' => '',
                    'sort_order' => '30',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'default' => null,
                    'visible' => true,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => true,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'unique' => false,
                    'apply_to' => 'simple,grouped,bundle,configurable,virtual',
                    'group' => 'Attributes',
                    'used_in_product_listing' => false,
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => true,
                    'is_html_allowed_on_front' => true
                ]
            );

            $eavSetup->removeAttribute(\Magento\Catalog\Model\Product::ENTITY, 'pre_order_to');
            $eavSetup->addAttribute(
                \Magento\Catalog\Model\Product::ENTITY,
                'pre_order_to',
                [
                    'type' => 'datetime',
                    'label' => 'Pre Order To',
                    'input' => 'date',
                    'source' => '',
                    'frontend' => '',
                    'required' => false,
                    'backend' => '',
                    'sort_order' => '30',
                    'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_STORE,
                    'default' => null,
                    'visible' => true,
                    'user_defined' => true,
                    'searchable' => false,
                    'filterable' => true,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'unique' => false,
                    'apply_to' => 'simple,grouped,bundle,configurable,virtual',
                    'group' => 'Attributes',
                    'used_in_product_listing' => false,
                    'is_used_in_grid' => true,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => true,
                    'is_html_allowed_on_front' => true
                ]
            );

            $attributeOptions = [
                'type'     => Table::TYPE_TEXT,
                'visible'  => true,
                'required' => false
            ];
            $quoteSetup->addAttribute('quote_item', 'stock_status', $attributeOptions);
            $salesSetup->addAttribute('order_item', 'stock_status', $attributeOptions);
        }
    }
}
