<?php
namespace Aureatelabs\PreOrderCustom\Setup;

use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Sales\Model\Order\StatusFactory;
use Magento\Sales\Model\ResourceModel\Order\StatusFactory as StatusResourceFactory;

class UpgradeData implements UpgradeDataInterface
{
    /* 
        CUSTOM_STATUS_CODE,CUSTOM_STATE_CODE and CUSTOM_STATUS_LABEL
    */
 
    const CUSTOM_STATUS_CODE = 'pre_ordered';
    const CUSTOM_STATE_CODE = 'pre_order';
    const CUSTOM_STATUS_LABEL = 'Pre Ordered';

    const PAID_BY_POINTS_STATUS_CODE = 'paid_by_points';
    const PAID_BY_POINTS_STATE_CODE = 'paid_by_points';
    const PAID_BY_POINTS_STATUS_LABEL = 'Paid by Points';
 
    protected $statusFactory;
    protected $statusResourceFactory;
 
    public function __construct(
        StatusFactory $statusFactory,
        StatusResourceFactory $statusResourceFactory
    ) {
        $this->statusFactory = $statusFactory;
        $this->statusResourceFactory = $statusResourceFactory;
    }
 
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.1') < 0) {
        $this->addCustomOrderStatus();
        }

        if ($context->getVersion() && version_compare($context->getVersion(), '1.0.2') < 0) {
            $this->addPaidByPointsOrderStatus();
        }
    }
 
    protected function addCustomOrderStatus()
    {
        $statusResource = $this->statusResourceFactory->create();
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => self::CUSTOM_STATUS_CODE,
            'label' => self::CUSTOM_STATUS_LABEL,
        ]);
        try {
            $statusResource->save($status);
        } catch (AlreadyExistsException $exception) {
            return;
        }
        $status->assignState(self::CUSTOM_STATE_CODE, true, true);
    }

    protected function addPaidByPointsOrderStatus()
    {
        $statusResource = $this->statusResourceFactory->create();
        $status = $this->statusFactory->create();
        $status->setData([
            'status' => self::PAID_BY_POINTS_STATUS_CODE,
            'label' => self::PAID_BY_POINTS_STATUS_LABEL,
        ]);
        try {
            $statusResource->save($status);
        } catch (AlreadyExistsException $exception) {
            return;
        }
        $status->assignState(self::PAID_BY_POINTS_STATE_CODE, true, true);
    }

}