<?php
namespace Aureatelabs\QuickLinks\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
		$installer = $setup;
		$installer->startSetup();

		/**
		 * Creating table aureatelabs_quicklinks
		 */
		$table = $installer->getConnection()->newTable(
			$installer->getTable('aureatelabs_quicklinks')
		)->addColumn(
			'quicklinks_id',
			\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			null,
			['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
			'Entity Id'
		)->addColumn(
			'label',
			\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
			255,
			['nullable' => true],
			'Label'
		)->addColumn(
			'link',
			\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
			255,
			['nullable' => true,'default' => null],
			'Link'
		)->addColumn(
			'include_in_menu',
			\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
			1,
			['nullable' => false,'default' => 0],
			'Status'
		)->addColumn(
			'include_in_footer',
			\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
			'2M',
			['nullable' => true,'default' => null],
			'Content'
		)->addColumn(
			'created_at',
			\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
			null,
			[
				'nullable' => false,
				'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,
			],
			'Created At'
		)->addColumn(
            'updated_at',
            \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
            null,
            [
				'nullable' => false,
				'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT,
			],
            'Updated At'
        )->setComment(
            'AureateLabs QuickLinks Table'
        );
		$installer->getConnection()->createTable($table);
		$installer->endSetup();
	}
}
