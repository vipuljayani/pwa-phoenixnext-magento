<?php
namespace Aureatelabs\QuickLinks\Controller\Adminhtml\Items;

class Index extends \Aureatelabs\QuickLinks\Controller\Adminhtml\Items
{
    /**
     * Items list.
     *
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Aureatelabs_QuickLinks::test');
        $resultPage->getConfig()->getTitle()->prepend(__('Quick links'));
        $resultPage->addBreadcrumb(__('Quick links'), __('Quick links'));
        $resultPage->addBreadcrumb(__('Items'), __('Items'));
        return $resultPage;
    }
}
