<?php
namespace Aureatelabs\QuickLinks\Controller\Adminhtml\Items;

class NewAction extends \Aureatelabs\QuickLinks\Controller\Adminhtml\Items
{

    public function execute()
    {
        $this->_forward('edit');
    }
}
