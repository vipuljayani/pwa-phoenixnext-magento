<?php
namespace Aureatelabs\QuickLinks\Model;

use Magento\Framework\Model\AbstractModel;

class QuickLinks extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Aureatelabs\QuickLinks\Model\ResourceModel\QuickLinks');
    }
}
