<?php

namespace Aureatelabs\QuickLinks\Model\Indexer;

use Divante\VsbridgeIndexerCore\Indexer\StoreManager;
use Aureatelabs\QuickLinks\Model\Indexer\Action\QuickLinks as QuickLinksAction;
use Divante\VsbridgeIndexerCore\Indexer\GenericIndexerHandler;

/**
 * Class QuickLinks
 * @package AureateLabs\QuickLinks\Model\Indexer
 */

class QuickLinks implements \Magento\Framework\Indexer\ActionInterface, \Magento\Framework\Mview\ActionInterface {

    /**
     * @var GenericIndexerHandler
     */
    private $indexHandler;

    /**
     * @var QuickLinksAction
     */
    private $quickLinksAction;

    /**
     * @var StoreManager
     */
    private $storeManager;

    /**
     * QuickLinks constructor.
     *
     * @param GenericIndexerHandler $indexerHandler
     * @param StoreManager $storeManager
     * @param QuickLinksAction $action
     */
    public function __construct(
        GenericIndexerHandler $indexerHandler,
        StoreManager $storeManager,
        QuickLinksAction $action
    ) {
        $this->indexHandler = $indexerHandler;
        $this->storeManager = $storeManager;
        $this->quickLinksAction = $action;
    }

    /*
     * Used by mview, allows process indexer in the "Update on schedule" mode
     * Used by mview, allows you to process multiple placed orders in the "Update on schedule" mode
     */
    public function execute($ids)
    {
        $stores = $this->storeManager->getStores();

        foreach ($stores as $store) {
            $this->indexHandler->saveIndex($this->quickLinksAction->rebuild($store->getId(), $ids), $store);
            $this->indexHandler->cleanUpByTransactionKey($store, $ids);
        }
    }

    /*
     * Will take all of the data and reindex
     * Will run when reindex via command line
     * Should take into account all placed orders in the system
     */
    public function executeFull()
    {
        $stores = $this->storeManager->getStores();

        foreach ($stores as $store) {
            $this->indexHandler->saveIndex($this->quickLinksAction->rebuild($store->getId()), $store);
            $this->indexHandler->cleanUpByTransactionKey($store);
        }
    }

    /*
     * Works with a set of entity changed (may be massaction)
     * Works with a set of placed orders (mass actions and so on)
     */
    public function executeList(array $ids)
    {
        $this->execute($ids);
    }

    /*
     * Works in runtime for a single entity using plugins
     * Works in runtime for a single order using plugins
     */
    public function executeRow($id)
    {
        $this->execute([$id]);
    }

}
