<?php

namespace Aureatelabs\QuickLinks\Model\Indexer\Action;

use Aureatelabs\QuickLinks\Model\ResourceModel\QuickLinks as QuickLinksResource;
use Magento\Framework\App\Area;
use Magento\Framework\App\AreaList;

class QuickLinks {

    /**
     * @var AreaList
     */
    private $areaList;

    /**
     * @var QuickLinksResource
     */
    private $resourceModel;

    /**
     * QuickLinks constructor.
     *
     * @param AreaList $areaList
     * @param QuickLinksResource $quickLinksResource
     * @param \Magento\Framework\Serialize\Serializer\Json $json
    */
    public function __construct(
        AreaList $areaList,
        QuickLinksResource $quickLinksResource
    ) {
        $this->areaList = $areaList;
        $this->resourceModel = $quickLinksResource;
    }

    /**
     * @param int $storeId
     * @param array $linkIds
     *
     * @return \Traversable
     * @throws \Exception
     */
    public function rebuild($storeId = 1, array $linkIds = []) {
        $this->areaList->getArea(Area::AREA_FRONTEND)->load(Area::PART_DESIGN);
        $links = $this->resourceModel->loadLinks($linkIds);

        foreach ($links as $link) {

            $lastLinkId = $link['quicklinks_id'];
            $link['id'] = $link['quicklinks_id'];

            yield $lastLinkId => $link;
        }
    }

}