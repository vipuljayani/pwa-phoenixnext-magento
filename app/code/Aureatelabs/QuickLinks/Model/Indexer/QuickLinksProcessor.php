<?php 

namespace Aureatelabs\QuickLinks\Model\Indexer;

class QuickLinksProcessor extends \Magento\Framework\Indexer\AbstractProcessor {

    /**
     * Indexer ID
     */
    const INDEXER_ID = 'vsbridge_quicklinks_indexer';

}