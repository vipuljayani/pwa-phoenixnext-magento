<?php
namespace Aureatelabs\QuickLinks\Model\ResourceModel;

class QuickLinks extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Define main table
     */

    protected $_date;

    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
        $this->_date = $date;
    }

    protected function _construct()
    {
        $this->_init('aureatelabs_quicklinks', 'quicklinks_id');   //here "aureatelabs_quicklinks" is table name and "quicklinks_id" is the primary key of custom table
    }

    public function loadLinks(array $linkIds = [], $fromId = 0, $limit = 1000) {
        $select = $this->getConnection()->select()->from(['link' => $this->getTable('aureatelabs_quicklinks')]);

        if (!empty($linkIds)) {
            $select->where('link.quicklinks_id IN (?)', $linkIds);
        }

        $select->where('link.quicklinks_id > ?', $fromId)
            ->limit($limit);

        return $this->getConnection()->fetchAll($select);

    }
}
