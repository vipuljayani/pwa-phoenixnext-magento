<?php
namespace Aureatelabs\QuickLinks\Model\ResourceModel\QuickLinks;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'quicklinks_id';
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Aureatelabs\QuickLinks\Model\QuickLinks',
            'Aureatelabs\QuickLinks\Model\ResourceModel\QuickLinks'
        );
    }
}
