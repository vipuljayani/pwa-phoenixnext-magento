<?php

namespace Aureatelabs\QuickLinks\Plugin\Indexer\QuickLinks\Save;

use Aureatelabs\QuickLinks\Model\Indexer\QuickLinksProcessor;
use Aureatelabs\QuickLinks\Model\QuickLinks;

class UpdateLinks {
    /**
     * @var QuickLinksProcessor
     */
    private $quickLinksProcessor;

    /**
     * Save constructor.
     *
     * @param QuickLinksProcessor $quickLinksProcessor
     */
    public function __construct(QuickLinksProcessor $quickLinksProcessor)
    {
        $this->quickLinksProcessor = $quickLinksProcessor;
    }

    public function afterAfterSave(QuickLinks $quickLinks, QuickLinks $result)
    {
        $result->getResource()->addCommitCallback(function () use ($quickLinks) {
            $this->quickLinksProcessor->reindexRow($quickLinks->getId());
        });

        return $result;
    }

    public function afterAfterDeleteCommit(QuickLinks $quickLinks, QuickLinks $result)
    {
        $this->quickLinksProcessor->reindexRow($quickLinks->getId());

        return $result;
    }
}