<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2020 Aureate Labs. ( https://aureatelabs.com )
 */

namespace Aureatelabs\Brands\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class UpgradeSchema
 * @package Aureatelabs\Brands\Setup
 */
class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        if (version_compare($context->getVersion(), '1.1.0', '<=')) {
            $this->addLinkFeature($setup);
        }
    }

    /**
     * @param $setup
     */
    public function addLinkFeature($setup)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('aureate_brands');

        if ($installer->getConnection()->isTableExists($tableName) == true) {

            $installer->getConnection()->addColumn(
                $tableName,
                'is_feature',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'default' => 0,
                    'comment' => 'Is Feature'
                ]
            );

            $installer->getConnection()->addColumn(
                $tableName,
                'link',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    'length' => 255,
                    'comment' => 'Link'
                ]
            );
        }

        $installer->endSetup();
    }

}