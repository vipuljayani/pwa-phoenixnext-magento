<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Brands\Controller\Adminhtml\Brand;

/**
 * Class Delete
 * @package Aureatelabs\Banner\Controller\Adminhtml\Banner
 */
class Delete extends \Aureatelabs\Brands\Controller\Adminhtml\Brand
{
    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($id) {
            $title = "";
            try {
                // init model and delete
                $model = $this->_objectManager->create(\Aureatelabs\Brands\Model\Brand::class);
                $model->load($id);

                $title = $model->getBrandName();
                $model->delete();

                $this->messageManager->addSuccessMessage(__('The brand has been deleted.'));
                $this->_eventManager->dispatch('aureate_brand_on_delete', [
                    'title' => $title,
                    'status' => 'success'
                ]);

                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {

                $this->_eventManager->dispatch('aureate_brand_on_delete', [
                    'title' => $title,
                    'status' => 'fail'
                ]);

                $this->messageManager->addErrorMessage($e->getMessage());
                return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
            }
        }

        $this->messageManager->addErrorMessage(__('We can\'t find a brand to delete.'));
        return $resultRedirect->setPath('*/*/');
    }
}
