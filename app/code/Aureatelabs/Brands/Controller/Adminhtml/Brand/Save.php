<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Brands\Controller\Adminhtml\Brand;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Save
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    protected $_date;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $date
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->_date =  $date;
        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        if ($data) {
            if (isset($data['brand_products']) && is_string($data['brand_products'])) {
            $selectedProducts = json_decode($data['brand_products'], true);        
                if(!empty($selectedProducts)){
                    $selectedProducts = array_keys($selectedProducts);
                
                    $selectedProductArray = array_filter($selectedProducts); 
                    
                    if (($key = array_search('Array', $selectedProductArray)) !== false) {
                        unset($selectedProductArray[$key]);
                    }
                    
                    if (!empty($selectedProductArray)) {
                        $pos = array_search('on', $selectedProductArray);
                        if ($pos) {
                            unset($selectedProductArray[$pos]);
                        }
                        $data['brand_products'] = implode(',', $selectedProductArray);
                    }
                } else {
                    $data['brand_products'] = '';
                }
            
            }else if(isset($data['brand_products'])){
                if (($key = array_search('Array', $data['brand_products'])) !== false) {
                    unset($data['brand_products'][$key]);
                }
                $data['brand_products'] = implode(',', $data['brand_products']);
            }

            if (isset($data['link']) && $data['link']) {
                $url = preg_replace('#[^0-9a-z]+#i', '', $data['link']);
                $urlKey = strtolower($url);

                $data['link'] = $urlKey;
            } else {
                $url = preg_replace('#[^0-9a-z]+#i', '', $data['brand_name']);
                $urlKey = strtolower($url);
                $data['link'] = $urlKey;
            }

            $data['updated_at'] = $this->_date->date()->format('Y-m-d H:i:s');

            $id = $this->getRequest()->getParam('id');

            $model = $this->_objectManager->create(\Aureatelabs\Brands\Model\Brand::class)->load($id);
            if (!$model->getId() && $id) {
                $this->messageManager->addErrorMessage(__('This Brand no longer exists.'));
                return $resultRedirect->setPath('*/*/');
            }
            $data = $this->_filterFaqGroupData($data);
            $model->setData($data);

            try {
                $model->save();
                $this->messageManager->addSuccessMessage(__('You saved the Brand.'));
                $this->dataPersistor->clear('aureate_brand_brand');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Brand.'));
            }

            $this->dataPersistor->set('aureate_brand_brand', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $this->getRequest()->getParam('id')]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param array $rawData
     * @return array
     */
    public function _filterFaqGroupData(array $rawData)
    {
        $data = $rawData;
        if (isset($data['brand_image'][0]['name'])) {
            $data['brand_image'] = $data['brand_image'][0]['name'];
        } else {
            $data['brand_image'] = null;
        }
        return $data;
    }
}
