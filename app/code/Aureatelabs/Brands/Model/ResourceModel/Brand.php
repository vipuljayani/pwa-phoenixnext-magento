<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Brands\Model\ResourceModel;

class Brand extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb {

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('aureate_brands', 'brand_id');
    }
     /**
     * @param int $storeId
     * @param array $brandIds
     * @param int $fromId
     * @param int $limit
     *
     * @return array
     * @throws \Exception
     */
    public function loadBrands($storeId = 0, array $brandIds = [], $fromId = 0, $limit = 1000)
    {
        $select = $this->getConnection()->select()->from(['brand' => $this->getTable('aureate_brands')]);
        if (!empty($brandIds)) {
            $select->where('brand.brand_id IN (?)', $brandIds);
        }

        $select->where('is_active = ?', 1);
        $select->where('brand.brand_id > ?', $fromId)
            ->limit($limit)
            ->order('brand.brand_id');

        return $this->getConnection()->fetchAll($select);
    }

}

