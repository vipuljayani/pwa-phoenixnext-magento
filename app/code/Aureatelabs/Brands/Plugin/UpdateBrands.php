<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Brand
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Brands\Plugin;

use Aureatelabs\Brands\Model\Indexer\BrandsProcessor;
use Aureatelabs\Brands\Model\Brand;

/**
 * Class UpdateBrand
 * @package Aureatelabs\Brands\Plugin
 */
class UpdateBrands
{
    /**
     * @var BrandsProcessor
     */
    private $brandProcessor;

    /**
     * UpdateBrand constructor.
     *
     * @param BrandsProcessor $brandProcessor
     */
    public function __construct(BrandsProcessor $brandProcessor)
    {
        $this->brandProcessor = $brandProcessor;
    }

    /**
     * @param Brand $brand
     * @param Brand $result
     *
     * @return Brand
     */
    public function afterAfterSave(Brand $brand, Brand $result)
    {
        $result->getResource()->addCommitCallback(function () use ($brand) {
            $this->brandProcessor->reindexRow($brand->getId());
        });

        return $result;
    }

    /**
     * @param Brand $brand
     * @param Brand $result
     *
     * @return Brand
     */
    public function afterAfterDeleteCommit(Brand $brand, Brand $result)
    {
        $this->brandProcessor->reindexRow($brand->getId());

        return $result;
    }
}
