<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\ProductRewardPoint\Api;

interface GetpointsManagementInterface
{

    /**
     * GET for getpoints api
     * @param string $product_id
     * @return string
     */
    public function getGetpoints($product_id);
}

