<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\ProductRewardPoint\Model;

class GetpointsManagement implements \Aureatelabs\ProductRewardPoint\Api\GetpointsManagementInterface
{

    /**
     * @param Earn $earnHelper
     * @param ProductRepository $productRepository
     * @param Session $customerSession
     * @param StoreManagerInterface $storeManager
     */

    public function __construct(
        \Mirasvit\RewardsCatalog\Helper\Earn $earnHelper,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ){
        $this->earnHelper = $earnHelper;
        $this->_productRepository = $productRepository;
        $this->customerSession    = $customerSession;
        $this->_storeManager = $storeManager;
    }
    /**
     * {@inheritdoc}
     */
    public function getGetpoints($product_id)
    {
        $product = $this->_productRepository->getById($product_id);
        $customer = $this->customerSession->getCustomer();
        $websiteId = $this->_storeManager->getDefaultStoreView()->getWebsiteId();
        $points = $this->earnHelper->getProductPoints($product, $customer, $websiteId);

        return $points;
    }
}

