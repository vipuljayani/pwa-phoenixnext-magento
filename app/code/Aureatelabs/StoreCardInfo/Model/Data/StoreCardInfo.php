<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\StoreCardInfo\Model\Data;

use Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface;

class StoreCardInfo extends \Magento\Framework\Api\AbstractExtensibleObject implements StoreCardInfoInterface
{

    /**
     * Get storecardinfo_id
     * @return string|null
     */
    public function getStorecardinfoId()
    {
        return $this->_get(self::STORECARDINFO_ID);
    }

    /**
     * Set storecardinfo_id
     * @param string $storecardinfoId
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setStorecardinfoId($storecardinfoId)
    {
        return $this->setData(self::STORECARDINFO_ID, $storecardinfoId);
    }

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId()
    {
        return $this->_get(self::CUSTOMER_ID);
    }

    /**
     * Set customer_id
     * @param string $customerId
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }

    /**
     * Get card_number
     * @return string|null
     */
    public function getCardNumber()
    {
        return $this->_get(self::CARD_NUMBER);
    }

    /**
     * Set card_number
     * @param string $cardNumber
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setCardNumber($cardNumber)
    {
        return $this->setData(self::CARD_NUMBER, $cardNumber);
    }

    /**
     * Get month
     * @return string|null
     */
    public function getMonth()
    {
        return $this->_get(self::MONTH);
    }

    /**
     * Set month
     * @param string $month
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setMonth($month)
    {
        return $this->setData(self::MONTH, $month);
    }

    /**
     * Get year
     * @return string|null
     */
    public function getYear()
    {
        return $this->_get(self::YEAR);
    }

    /**
     * Set year
     * @param string $year
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setYear($year)
    {
        return $this->setData(self::YEAR, $year);
    }
}

