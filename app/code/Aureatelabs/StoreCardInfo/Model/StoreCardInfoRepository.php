<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\StoreCardInfo\Model;

use Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterfaceFactory;
use Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoSearchResultsInterfaceFactory;
use Aureatelabs\StoreCardInfo\Api\StoreCardInfoRepositoryInterface;
use Aureatelabs\StoreCardInfo\Model\ResourceModel\StoreCardInfo as ResourceStoreCardInfo;
use Aureatelabs\StoreCardInfo\Model\ResourceModel\StoreCardInfo\CollectionFactory as StoreCardInfoCollectionFactory;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;

class StoreCardInfoRepository implements StoreCardInfoRepositoryInterface
{

    private $collectionProcessor;

    protected $dataObjectHelper;

    protected $storeCardInfoCollectionFactory;

    protected $storeCardInfoFactory;

    protected $extensionAttributesJoinProcessor;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;

    protected $dataStoreCardInfoFactory;

    private $storeManager;


    /**
     * @param ResourceStoreCardInfo $resource
     * @param StoreCardInfoFactory $storeCardInfoFactory
     * @param StoreCardInfoInterfaceFactory $dataStoreCardInfoFactory
     * @param StoreCardInfoCollectionFactory $storeCardInfoCollectionFactory
     * @param StoreCardInfoSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceStoreCardInfo $resource,
        StoreCardInfoFactory $storeCardInfoFactory,
        StoreCardInfoInterfaceFactory $dataStoreCardInfoFactory,
        StoreCardInfoCollectionFactory $storeCardInfoCollectionFactory,
        StoreCardInfoSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->storeCardInfoFactory = $storeCardInfoFactory;
        $this->storeCardInfoCollectionFactory = $storeCardInfoCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataStoreCardInfoFactory = $dataStoreCardInfoFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface $storeCardInfo
    ) {
        /* if (empty($storeCardInfo->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $storeCardInfo->setStoreId($storeId);
        } */
        
        $storeCardInfoData = $this->extensibleDataObjectConverter->toNestedArray(
            $storeCardInfo,
            [],
            \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface::class
        );
        
        $storeCardInfoModel = $this->storeCardInfoFactory->create()->setData($storeCardInfoData);
        
        try {
            $this->resource->save($storeCardInfoModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the storeCardInfo: %1',
                $exception->getMessage()
            ));
        }
        return $storeCardInfoModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($storeCardInfoId)
    {
        $storeCardInfo = $this->storeCardInfoFactory->create();
        $this->resource->load($storeCardInfo, $storeCardInfoId);
        if (!$storeCardInfo->getId()) {
            throw new NoSuchEntityException(__('StoreCardInfo with id "%1" does not exist.', $storeCardInfoId));
        }
        return $storeCardInfo->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->storeCardInfoCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface $storeCardInfo
    ) {
        try {
            $storeCardInfoModel = $this->storeCardInfoFactory->create();
            $this->resource->load($storeCardInfoModel, $storeCardInfo->getStorecardinfoId());
            $this->resource->delete($storeCardInfoModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the StoreCardInfo: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($storeCardInfoId)
    {
        return $this->delete($this->get($storeCardInfoId));
    }
}

