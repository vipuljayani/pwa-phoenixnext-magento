<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\StoreCardInfo\Model;

use Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface;
use Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterfaceFactory;
use Magento\Framework\Api\DataObjectHelper;

class StoreCardInfo extends \Magento\Framework\Model\AbstractModel
{

    protected $_eventPrefix = 'aureatelabs_storecardinfo_storecardinfo';
    protected $dataObjectHelper;

    protected $storecardinfoDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param StoreCardInfoInterfaceFactory $storecardinfoDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Aureatelabs\StoreCardInfo\Model\ResourceModel\StoreCardInfo $resource
     * @param \Aureatelabs\StoreCardInfo\Model\ResourceModel\StoreCardInfo\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        StoreCardInfoInterfaceFactory $storecardinfoDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Aureatelabs\StoreCardInfo\Model\ResourceModel\StoreCardInfo $resource,
        \Aureatelabs\StoreCardInfo\Model\ResourceModel\StoreCardInfo\Collection $resourceCollection,
        array $data = []
    ) {
        $this->storecardinfoDataFactory = $storecardinfoDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve storecardinfo model with storecardinfo data
     * @return StoreCardInfoInterface
     */
    public function getDataModel()
    {
        $storecardinfoData = $this->getData();
        
        $storecardinfoDataObject = $this->storecardinfoDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $storecardinfoDataObject,
            $storecardinfoData,
            StoreCardInfoInterface::class
        );
        
        return $storecardinfoDataObject;
    }
}

