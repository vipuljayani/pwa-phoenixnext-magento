<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\StoreCardInfo\Api\Data;

interface StoreCardInfoInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const CUSTOMER_ID = 'customer_id';
    const STORECARDINFO_ID = 'storecardinfo_id';
    const CARD_NUMBER = 'card_number';
    const MONTH = 'month';
    const YEAR = 'year';

    /**
     * Get storecardinfo_id
     * @return string|null
     */
    public function getStorecardinfoId();

    /**
     * Set storecardinfo_id
     * @param string $storecardinfoId
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setStorecardinfoId($storecardinfoId);

    /**
     * Get customer_id
     * @return string|null
     */
    public function getCustomerId();

    /**
     * Set customer_id
     * @param string $customerId
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setCustomerId($customerId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoExtensionInterface $extensionAttributes
    );

    /**
     * Get card_number
     * @return string|null
     */
    public function getCardNumber();

    /**
     * Set card_number
     * @param string $cardNumber
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setCardNumber($cardNumber);

    /**
     * Get month
     * @return string|null
     */
    public function getMonth();

    /**
     * Set month
     * @param string $month
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setMonth($month);

    /**
     * Get year
     * @return string|null
     */
    public function getYear();

    /**
     * Set year
     * @param string $year
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     */
    public function setYear($year);
}

