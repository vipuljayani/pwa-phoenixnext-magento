<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\StoreCardInfo\Api\Data;

interface StoreCardInfoSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get StoreCardInfo list.
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface[]
     */
    public function getItems();

    /**
     * Set customer_id list.
     * @param \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

