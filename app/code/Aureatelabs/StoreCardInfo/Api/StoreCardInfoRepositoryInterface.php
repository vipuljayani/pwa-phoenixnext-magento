<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\StoreCardInfo\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface StoreCardInfoRepositoryInterface
{

    /**
     * Save StoreCardInfo
     * @param \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface $storeCardInfo
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface $storeCardInfo
    );

    /**
     * Retrieve StoreCardInfo
     * @param string $storecardinfoId
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($storecardinfoId);

    /**
     * Retrieve StoreCardInfo matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete StoreCardInfo
     * @param \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface $storeCardInfo
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Aureatelabs\StoreCardInfo\Api\Data\StoreCardInfoInterface $storeCardInfo
    );

    /**
     * Delete StoreCardInfo by ID
     * @param string $storecardinfoId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storecardinfoId);
}

