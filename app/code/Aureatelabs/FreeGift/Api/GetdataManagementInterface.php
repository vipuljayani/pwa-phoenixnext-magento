<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\FreeGift\Api;

interface GetdataManagementInterface
{

    /**
     * GET for getdata api
     * @param string $quote_id
     * @return string
     */
    public function getGetdata($quote_id);
}

