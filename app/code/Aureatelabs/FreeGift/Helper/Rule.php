<?php
namespace Aureatelabs\FreeGift\Helper;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable as TypeConfigurable;
use Mageplaza\FreeGifts\Model\Source\Type as RuleType;

class Rule extends \Mageplaza\FreeGifts\Helper\Rule
{
    /**
     * @param RuleModel $rule
     * @param bool $collectTotal
     *
     * @return array
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function processRuleData(\Mageplaza\FreeGifts\Model\Rule $rule, $collectTotal = false)
    {
        $gifts      = $rule->getGiftArray();
        $totalAdded = 0;
        foreach ($gifts as $id => $gift) {
            if ($this->_helperGift->isGiftInStock($id)) {
                $productGift  = $this->_helperGift->getProductById($id);
                $giftPrice    = $this->_helperGift->getGiftPrice(
                    $gift['discount'],
                    $gift['gift_price'],
                    $productGift->getFinalPrice()
                );
                $configurable = $productGift->getTypeId() === TypeConfigurable::TYPE_CODE;
                $giftAdded    = $this->_helperGift->isGiftAdded($id, $this->getQuote()->getId());

                if (is_array($giftAdded)) {
                    $gifts[$id]['added_options'] = $giftAdded;
                    $giftAdded                   = true;
                }
                if ($giftAdded) {
                    $totalAdded++;
                }

                $gifts[$id]['id']         = $id;
                $gifts[$id]['free_ship']  = (int) $gift['free_ship'];
                $gifts[$id]['gift_price'] = $this->getExtraData()
                    ? $this->_helperData->formatPrice($giftPrice)
                    : $giftPrice;

                if ($this->getExtraData()) {
                    $gifts[$id]['added']           = $giftAdded;
                    $gifts[$id]['configurable']    = $configurable;
                    $gifts[$id]['required_option'] = (int) $productGift->getRequiredOptions() ? true : false;
                    $gifts[$id]['name']            = $productGift->getName();
                    $gifts[$id]['final_price']     = $this->_helperData->formatPrice($productGift->getFinalPrice());
                    $gifts[$id]['image']           = $this->getHelperGift()->getGiftImage($productGift);
                    $gifts[$id]['author']          = $productGift->getAttributeText('author');
                    $gifts[$id]['number_of_pages'] =  $productGift->getResource()->getAttribute('number_of_pages')->getFrontend()->getValue($productGift);
                    $gifts[$id]['sku'] =  $productGift->getSku();
                }

                unset($gifts[$id]['discount']);
            } else {
                unset($gifts[$id]);
            }
        }

        $ruleData = [
            'rule_id'     => $rule->getId(),
            'auto_add'    => $rule->getType() === RuleType::AUTOMATIC ? 1 : 0,
            'max_gift'    => $rule->getMaxGift() > count($gifts) ? count($gifts) : $rule->getMaxGift(),
            'gifts'       => $collectTotal ? $gifts : array_values($gifts),
            'total_added' => $totalAdded,
        ];

        if ($rule->isAllowNotice()) {
            $ruleData['notice'] = $rule->getNoticeContent();
        }

        return $ruleData;

        //return $ruleData;
    }
}
