<?php
namespace Aureatelabs\RewardsPoint\Plugin;

use Mirasvit\Rewards\Helper\Balance;

/**
 * Update customer tier after customer balance was changed
 */
class RewardPointGet
{
    /**
     * @var TierInterface
     */
    private $tierService;

     /**
     * @var BalanceInterface
     */
    private $balance;

    protected $_customerRepositoryInterface;

    public function __construct(
        Balance $balance
    ) {
        $this->balance = $balance;
    }

    /**
     * @param subject   $subject
     * @param customerId $procecustomerIded
     * @return string
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aftergetById(\Magento\Customer\Api\CustomerRepositoryInterface $subject, $result,$customerId)
    {   
        $result = $this->saveRewardspointAttribute($result);         
        return $result;
    }

    private function saveRewardspointAttribute(\Magento\Customer\Api\Data\CustomerInterface $customer)
    {
       
        $extensionAttributes = $customer->getExtensionAttributes();
        $extensionAttributes->setRewardspoint($this->balance->getBalancePoints($customer));
        $customer->setExtensionAttributes($extensionAttributes);
        return $customer;
    }
}