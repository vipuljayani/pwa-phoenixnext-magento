<?php
/**
 * Copyright © 2020 All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Aureatelabs\RewardsPoint\Api;

interface GetRewardsHistoryInterface
{

    /**
     * GET for getRewardsHistory api
     * @param int $customer_id
     * @return string
     */
    public function getRewardsHistory($customer_id);
}