<?php

namespace Aureatelabs\SocialSignup\Model;

class Response extends \Magento\Framework\Model\AbstractExtensibleModel implements \Aureatelabs\SocialSignup\Api\ResponseInterface
{
    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->getData(self::MESSAGE);
    }

    /**
     * @param string $msg
     * @return $this
     */
    public function setMessage($msg)
    {
        $this->setData(self::MESSAGE, $msg);
        return $this;
    }

    /**
     * @return bool
     */
    public function getStatus()
    {
        return $this->getData(self::STATUS);
    }

    /**
     * @param string $msg
     * @return $this
     */
    public function setStatus($status)
    {
        $this->setData(self::STATUS, $status);
        return $this;
    }
}