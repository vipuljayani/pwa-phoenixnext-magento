<?php

namespace Aureatelabs\SocialSignup\Model;

use Magento\Integration\Model\Oauth\TokenFactory as TokenModelFactory;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Filter\Input\MaliciousCode;

/**
* @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
class SocialSignup implements \Aureatelabs\SocialSignup\Api\SocialSignupInterface
{
    /**
     * Token Model
     *
     * @var TokenModelFactory
     */
    private $tokenModelFactory;

    /**
     * Customer model factory
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $_customerFactory;

    /**
     * Customer repository
     * @var \Magento\Customer\Model\ResourceModel\CustomerRepository
     */
    protected $_customerRepository;

    /**
     * @var \Magento\Customer\Model\ResourceModel\Customer
     */
    protected $_customerRes;

    /**
     * @var \Aureatelabs\SocialSignup\Api\ResponseFactory
     */
    protected $_responseFactory;

    /**
     * @var MaliciousCode
     */
    protected $maliciousCode;

    public function __construct(
        TokenModelFactory $tokenModelFactory,
        \Magento\Customer\Model\ResourceModel\Customer $customerRes,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\ResourceModel\CustomerRepository $customerRepository,
        \Aureatelabs\SocialSignup\Model\ResponseFactory $responseFactory,
        MaliciousCode $maliciousCode
    ) {
        $this->tokenModelFactory = $tokenModelFactory;
        $this->_customerFactory = $customerFactory;
        $this->_customerRepository = $customerRepository;
        $this->_customerRes = $customerRes;
        $this->_responseFactory = $responseFactory;
        $this->maliciousCode = $maliciousCode;
    }

    /**
     * @inheritdoc
     */
    public function signup(int $customerId, $facebookId = null, $googleId = null)
    {
        try {
            $this->_customerRepository->getById($customerId);
            if ($googleId) {
                $sql = "UPDATE `customer_entity` SET `google_id` = :google_id WHERE `entity_id` = :entity_id ;";
                $this->_customerRes->getConnection()->query($sql, [
                'google_id' => $googleId,
                'entity_id' => $customerId
            ]);
            } else if ($facebookId) {
                $sql = "UPDATE `customer_entity` SET `facebook_id` = :facebook_id WHERE `entity_id` = :entity_id ;";
                $this->_customerRes->getConnection()->query($sql, [
                    'facebook_id' => $facebookId,
                    'entity_id' => $customerId
                ]);
            }
            $response = $this->_responseFactory->create();
            $response->setMessage('saved data to customer')->setStatus(true);
            return $response;
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        } catch (\Exception $e) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        }

        throw new AuthenticationException(
            __(
                'The account sign-in was incorrect or your account is disabled temporarily. '
                . 'Please wait and try again later.'
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function signin($facebookId = null, $googleId = null)
    {
        if (empty(trim($facebookId)) && empty(trim($googleId))) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        }
        $result = null;
        try {
            $sql = "SELECT `entity_id` AS `id` FROM `customer_entity`";
            $where = '';
            $bind = [];
            if ($facebookId) {
                $where .= " WHERE `facebook_id` = :facebook_id";
                $bind = [
                    'facebook_id' => $facebookId
                ];
            } elseif ($googleId) {
                $where .= " WHERE `google_id` = :google_id";
                $bind = [
                    'google_id' => $googleId
                ];
            }
            $id = $this->_customerRes->getConnection()->fetchOne($sql . $where, $bind);
            if ($id) {
                $token = $this->tokenModelFactory->create()->createCustomerToken($id)->getToken();
                return $token;
            } else {
                throw new AuthenticationException(
                    __(
                        'The account sign-in was incorrect or your account is disabled temporarily. '
                        . 'Please wait and try again later.'
                    )
                );
            }
        } catch (AuthenticationException $e) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        } catch (\Exception $e) {
            throw new AuthenticationException(
                __(
                    'The account sign-in was incorrect or your account is disabled temporarily. '
                    . 'Please wait and try again later.'
                )
            );
        }
    }

    /**
     * @inheritdoc
     */
    public function newsignup($firstname, $lastname, $email, $type, $typeId)
    {
        if ($type != 'google' && $type != 'facebook') {
            throw new AuthenticationException(__('Invalid data request found.'));
        }

        $obj = \Magento\Framework\App\ObjectManager::getInstance();
        $store = $obj->create(\Magento\Store\Model\StoreManagerInterface::class);

        // Get Website ID
        $websiteId  = $store->getWebsite()->getWebsiteId();
        $customer = $this->_customerFactory->create();
        $customer->setWebsiteId($websiteId);

        // Preparing data for new customer
        $customer->setEmail($email);
        $customer->setFirstname($firstname);
        $customer->setLastname($lastname);
        $customer->setPassword("DEFAULTPASSWORD");

        try {
            // Save data
            $customer = $customer->save();
        } catch (AlreadyExistsException $e) {
            return $this->updateCustomer($type, $typeId, $email);
        }

        //$customer->sendNewAccountEmail();

        return $this->updateCustomer($type, $typeId, $email);
    }

    /**
     * Update Google Id/Facebook Id for the customer by email address
     */
    public function updateCustomer($type, $typeId, $email)
    {
        if ($type == 'google') {

            $sql = "UPDATE `customer_entity` SET `google_id` = :google_id WHERE `email` = :email ;";
            $this->_customerRes->getConnection()->query($sql, [
                'google_id' => $typeId,
                'email' => $email
            ]);

            return $this->signin(null, $typeId);
        } else if ($type == 'facebook') {

            $sql = "UPDATE `customer_entity` SET `facebook_id` = :facebook_id WHERE `email` = :email ;";
            $this->_customerRes->getConnection()->query($sql, [
                'facebook_id' => $typeId,
                'email' => $email
            ]);

            return $this->signin($typeId, null);
        }
    }
}
