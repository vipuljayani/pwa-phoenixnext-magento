<?php

namespace Aureatelabs\SocialSignup\Api;

/**
* @SuppressWarnings(PHPMD.CouplingBetweenObjects)
*/
interface ResponseInterface
{
    const MESSAGE = 'message';
    const STATUS = 'status';
    /**
     * @return string
     */
    public function getMessage();

    /**
     * @param string $msg
     * @return $this
     */
    public function setMessage($msg);

    /**
     * @return bool
     */
    public function getStatus();

    /**
     * @param bool $msg
     * @return $this
     */
    public function setStatus($status);
}
