<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Banner\Api\Data;

/**
 * Interface BannerInterface
 * @package Aureatelabs\Banner\Api\Data
 */
interface BannerInterface
{
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case.
     */
    const ENTITY_ID = 'entity_id';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const LINK = 'link';
    const IMAGE = 'image';
    const IS_ACTIVE = 'is_active';
    const SORT_ORDER = 'sort_order';
    const UPDATE_AT = 'updated_at';
    const CREATED_AT = 'created_at';

    /**
     * Get EntityId.
     *
     * @return int
     */
    public function getEntityId();

    /**
     * Set EntityId.
     */
    public function setEntityId($entityId);

    /**
     * Get Title.
     *
     * @return varchar
     */
    public function getTitle();

    /**
     * Set Title.
     */
    public function setTitle($title);

    /**
     * Get Description.
     *
     * @return varchar
     */
    public function getDescription();

    /**
     * Set Description.
     */
    public function setDescription($description);

    /**
     * Get Link.
     *
     * @return varchar
     */
    public function getLink();

    /**
     * Set Link.
     */
    public function setLink($link);

    /**
     * Get Image.
     *
     * @return varchar
     */
    public function getImage();

    /**
     * Set Image.
     */
    public function setImage($image);

    /**
     * Get IsActive.
     *
     * @return varchar
     */
    public function getIsActive();

    /**
     * Set IsActive.
     */
    public function setIsActive($isActive);

    /**
     * Get Sort Order.
     *
     * @return varchar
     */
    public function getSortOrder();

    /**
     * Set Sort Order.
     */
    public function setSortOrder($sortOrder);

    /**
     * Get CreatedAt.
     *
     * @return varchar
     */
    public function getCreatedAt();

    /**
     * Set CreatedAt.
     */
    public function setCreatedAt($createdAt);

    /**
     * Get UpdateTime.
     *
     * @return varchar
     */
    public function getUpdatedAt();

    /**
     * Set UpdateTime.
     */
    public function setUpdatedAt($updatedAt);
}
