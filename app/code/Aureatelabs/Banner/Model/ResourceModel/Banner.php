<?php
/**
 * Aureate Labs Pvt Ltd.
 *
 * Do not edit or add to this file if you wish to upgrade to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please contact us https://aureatelabs.com/contact-us.
 *
 * @category   Aureatelabs
 * @package    Aureatelabs_Banner
 * @author     Aureate Labs Team
 * @copyright  Copyright (c) 2019 Aureate Labs. ( https://aureatelabs.com )
 */
namespace Aureatelabs\Banner\Model\ResourceModel;

/**
 * Class Banner
 * @package Aureatelabs\Banner\Model\ResourceModel
 */
class Banner extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * Banner constructor.
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param null $resourcePrefix
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
        $this->_date = $date;
    }

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('aureate_banner', 'entity_id');
    }

    /**
     * @param int $storeId
     * @param array $bannerIds
     * @param int $fromId
     * @param int $limit
     *
     * @return array
     * @throws \Exception
     */
    public function loadBanners($storeId = 0, array $bannerIds = [], $fromId = 0, $limit = 1000)
    {
        $select = $this->getConnection()->select()->from(['banner' => $this->getTable('aureate_banner')]);
        if (!empty($bannerIds)) {
            $select->where('banner.entity_id IN (?)', $bannerIds);
        }

        $select->where('banner.store_id IN (?)', [0, $storeId]);

        $select->where('is_active = ?', 1);
        $select->where('banner.entity_id > ?', $fromId)
            ->limit($limit)
            ->order('banner.sort_order');

        return $this->getConnection()->fetchAll($select);
    }
}
