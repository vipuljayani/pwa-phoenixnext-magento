<?php
/**
 * Copyright © Auteatelabs All rights reserved.
 * See COPYING.txt for license details.
 */
use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Aureatelabs_CategoryAttributes', __DIR__);

