<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   3.0.7
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */


namespace Mirasvit\Rewards\Helper\Balance\Spend;

use Mirasvit\Rewards\Api\Config\Rule\SpendingStyleInterface;
use Mirasvit\Rewards\Helper\Calculation;

class ChargesCalc
{
    private $config;
    private $shippingService;

    public function __construct(
        \Mirasvit\Rewards\Model\Config $config,
        \Mirasvit\Rewards\Service\ShippingService $shippingService
    ) {
        $this->config          = $config;
        $this->shippingService = $shippingService;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param float $subtotal
     * @return float
     */
    public function applyAdditionalCharges($quote, $subtotal)
    {
        if ((!$quote->getAwUseStoreCredit() || $quote->getIncludeSurcharge()) &&
            $quote->getBaseMagecompSurchargeAmount() && $subtotal
        ) {
            $subtotal += $quote->getBaseMagecompSurchargeAmount();
        }
        if ($quote->getAwUseStoreCredit() &&
            !$quote->getIncludeSurcharge() &&
            $subtotal >= $quote->getBaseMagecompSurchargeAmount()
        ) {
            $subtotal -= $quote->getBaseMagecompSurchargeAmount();
        }
        if ($subtotal < 0) { // compatibility with Aheadworks Store Credit
            $subtotal = 0;
        }
        return $subtotal;
    }

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @return float
     */
    public function getShippingAmount($quote)
    {
        if ($this->config->getGeneralIsSpendShipping() && !$quote->isVirtual()) {
            return $this->shippingService->getShippingAmount();
        }
        return 0;
    }

}