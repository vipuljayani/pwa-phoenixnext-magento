<?php
/**
 * Mirasvit
 *
 * This source file is subject to the Mirasvit Software License, which is available at https://mirasvit.com/license/.
 * Do not edit or add to this file if you wish to upgrade the to newer versions in the future.
 * If you wish to customize this module for your needs.
 * Please refer to http://www.magentocommerce.com for more information.
 *
 * @category  Mirasvit
 * @package   mirasvit/module-rewards
 * @version   3.0.7
 * @copyright Copyright (C) 2020 Mirasvit (https://mirasvit.com/)
 */



namespace Mirasvit\Rewards\Helper;

use Mirasvit\Rewards\Model\Config as Config;

class Referral extends \Magento\Framework\App\Helper\AbstractHelper
{
    private $referralService;
    /**
     * @var \Mirasvit\Rewards\Model\ReferralFactory
     */
    protected $referralFactory;
    /**
     * @var \Magento\Customer\Model\CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var \Magento\Store\Model\StoreFactory
     */
    protected $storeFactory;
    /**
     * @var \Mirasvit\Rewards\Model\ResourceModel\Referral\CollectionFactory
     */
    protected $referralCollectionFactory;
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;
    /**
     * @var BehaviorRule
     */
    protected $rewardsBehavior;
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\App\Helper\Context
     */
    protected $context;


    public function __construct(
        \Mirasvit\Rewards\Model\ReferralFactory $referralFactory,
        \Mirasvit\RewardsApi\Service\Referral $referralService,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Store\Model\StoreFactory $storeFactory,
        \Mirasvit\Rewards\Model\ResourceModel\Referral\CollectionFactory $referralCollectionFactory,
        \Magento\Customer\Model\Session $session,
        \Mirasvit\Rewards\Helper\BehaviorRule $rewardsBehavior,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\Helper\Context $context
    ) {
        $this->referralService           = $referralService;
        $this->referralFactory           = $referralFactory;
        $this->customerFactory           = $customerFactory;
        $this->storeFactory              = $storeFactory;
        $this->referralCollectionFactory = $referralCollectionFactory;
        $this->session                   = $session;
        $this->rewardsBehavior           = $rewardsBehavior;
        $this->storeManager              = $storeManager;
        $this->context                   = $context;

        parent::__construct($context);
    }

    /**
     * @param \Magento\Customer\Model\Customer $customer
     * @param array                            $invitations
     * @param string                           $message
     * @return array
     */
    public function frontendPost($customer, $invitations, $message)
    {
        $referrals = $this->referralCollectionFactory->create()
                ->addFieldToFilter("customer_id", $customer->getId())
                ->addFieldToFilter("created_at", ["gt"=> new \Zend_Db_Expr('DATE_SUB(NOW(), INTERVAL 1 DAY)')])
                ;
        if ($referrals->count() > 100) { //protection
            return [];
        }

        $rejectedEmails = [];
        foreach ($invitations as $email => $name) {
            $referrals = $this->referralCollectionFactory->create()
                ->addFieldToFilter('email', $email);
            if ($referrals->count()) {
                $rejectedEmails[] = $email;
                continue;
            }

            $message = nl2br(strip_tags($message));

            /** @var  \Mirasvit\Rewards\Model\Referral $referral */
            $referral = $this->referralFactory->create()
                        ->setName($name)
                        ->setEmail($email)
                        ->setCustomerId($customer->getId())
                        ->setStoreId($this->storeManager->getStore()->getId())
                        ->save();
            $referral->sendInvitation($message);

            $this->rewardsBehavior->processRule(Config::BEHAVIOR_TRIGGER_SEND_LINK, $customer, $this->storeManager->getWebsite()->getId(), $email);
        }

        return $rejectedEmails;
    }

    /**
     * Remember refrerer when customer adds product to cart.
     * @todo check that this function is needed
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return void
     */
    public function rememberReferal($quote)
    {
        //if we have referral, we save quote id
        if ($id = (int) $this->session->getReferral()) {
            $referral = $this->referralFactory->create()->load($id);
            if (!$referral->getQuoteId()) {
                $referral->setQuoteId($quote->getId());
                $referral->save();
            }
        }
    }

    /**
     * Find possible \Mirasvit\Rewards\Model\Referral for this order.
     *
     * @param \Magento\Sales\Model\Order $order
     *
     * @return \Mirasvit\Rewards\Model\Referral|bool
     */
    public function loadReferral($order)
    {
        $quoteId = $order->getQuoteId();
        $referrals = $this->referralCollectionFactory->create()
                        ->addFieldToFilter('quote_id', $quoteId);
        if ($referrals->count()) {
            return $referrals->getFirstItem();
        }

        $referrals = $this->referralCollectionFactory->create()
            ->addFieldToFilter('email', $order->getCustomerEmail());
        if ($referrals->count()) {
            return $referrals->getFirstItem();
        }

        $customerId = $order->getCustomerId();
        $referrals = $this->referralCollectionFactory->create()
                        ->addFieldToFilter('new_customer_id', $customerId);
        if ($referrals->count()) {
            return $referrals->getFirstItem();
        }

        return false;
    }

    /**
     * Customer A refers customer B. Customer B has placed this order.
     * This function can give points to customer A.
     *
     * @param \Magento\Sales\Model\Order $order
     *
     * @return void
     * @todo replace with \Mirasvit\Rewards\Service\Order\Transaction\Earn\Referral
     */
    public function processReferralOrder($order)
    {
        if (!$referral = $this->loadReferral($order)) {
            return;
        }
        /* @var  \Magento\Customer\Model\Customer $customer - customer A */
        if ($customerId = $order->getCustomerId()) {
            $customer = $this->customerFactory->create()->load($customerId);
        } else {
            $customer = new \Magento\Framework\DataObject();
            $customer->setIsGuest(true)
                    ->setEmail($order->getCustomerEmail())
                    ->setFirstname($order->getCustomerFirstname())
                    ->setLastname($order->getCustomerLastname());
        }

        $websiteId = $this->storeFactory->create()->load($order->getStoreId())->getWebsiteId();
        $transaction = $this->rewardsBehavior->processRule(
            Config::BEHAVIOR_TRIGGER_REFERRED_CUSTOMER_ORDER,
            $referral->getCustomerId(),
            $websiteId,
            $order->getId(),
            ['referred_customer' => $customer, 'order' => $order]
        );
        $referral->finish(Config::BEHAVIOR_TRIGGER_REFERRED_CUSTOMER_ORDER, $customerId, $transaction);
    }

    /**
     *
     * @return string
     */
    public function getReferralLinkId()
    {
        $customerId = $this->session->getCustomerId();

        return $this->referralService->getReferralCode($customerId);
    }
}
