<?php
/**
* CURL POST
* @param string $url
* @param string $fields_string
* @return void
* @author 2c2p
*/
namespace P2c2p\P2c2pPayment\Helper;

use Zend\Http\Client;

Class HTTP {
       function post($url,$fields_string)
       {
            try{//open connection
              /*$ch = curl_init();
              curl_setopt($ch,CURLOPT_URL, $url);
              curl_setopt($ch,CURLOPT_POSTFIELDS, $fields_string);
              curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);*/
              //execute post
              /*$result = curl_exec($ch); //close connection
              echo $result;*/
              //curl_close($ch);
              $client = new Client();
              $client->setUri($url,array(
                 'adapter' => 'Zend\Http\Client\Adapter\Curl'
              ));
              $client->setMethod('POST');
              $client->setParameterPost($fields_string);

              $response = $client->send();

              if ($response->isSuccess()) {
                  return $response;
              }
              return "";
            }catch(Exception $e) {
              echo $e->errorMessage();
              return $e;
            }
    }
    function curlPost($url,$fields_string)
    {
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
      curl_setopt($ch, CURLOPT_POSTFIELDS,  $fields_string);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

      $result = curl_exec($ch);
      return $result;
    }
}
