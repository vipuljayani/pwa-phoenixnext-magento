<?php

/*
 * Created by 2C2P
 * Date 19 June 2017
 * P2c2pRequest helper class is used to generate the current user request and send it to 2c2p payment getaway.
 */

namespace P2c2p\P2c2pPayment\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use P2c2p\P2c2pPayment\Helper\P2c2pHash;
use P2c2p\P2c2pPayment\Helper\P2c2pCurrencyCode;
use Magento\Store\Model\StoreManagerInterface;
use P2c2p\P2c2pPayment\Helper\HTTP;
use P2c2p\P2c2pPayment\Helper\pkcs7;

class P2c2pRequest extends AbstractHelper{

	private $objConfigSettings;
	private $objP2c2pHashHelper;
	private $objStoreManagerInterface;
	private $objP2c2pCurrencyCodeHelper;

	function __construct(ScopeConfigInterface $configSettings, P2c2pHash $p2c2pHash,
				StoreManagerInterface $storeManagerInterface,
				P2c2pCurrencyCode $p2c2pCurrencyCode) {

		$this->objConfigSettings = $configSettings->getValue('payment/p2c2ppayment');
		$this->objP2c2pHashHelper = $p2c2pHash;
		$this->objStoreManagerInterface = $storeManagerInterface;
		$this->objP2c2pCurrencyCodeHelper = $p2c2pCurrencyCode;
	}

	//Declare the Form array to hold the 2c2p form request.
	private $arrayP2c2pFormFields = array(
		"version"     			=> "",
		"merchant_id" 			=> "",
		"payment_description" 	=> "",
		"order_id" 				=> "",
		"invoice_no" 			=> "",
		"currency" 				=> "",
		"amount" 				=> "",
		"customer_email" 		=> "",
		"pay_category_id" 		=> "",
		"promotion" 			=> "",
		"user_defined_1" 		=> "",
		"user_defined_2" 		=> "",
		"user_defined_3" 		=> "",
		"user_defined_4" 		=> "",
		"user_defined_5" 		=> "",
		"result_url_1" 			=> "",
		"result_url_2" 			=> "",
		"payment_option" 		=> "",
		"enable_store_card" 	=> "",
		"stored_card_unique_id" => "",
		"request_3ds"   		=> "",
		"payment_expiry" 		=> "",
		"default_lang" 			=> "",
		"statement_descriptor" 	=> "",
		"hash_value" 			=> ""
		);

	//This function is used to genereate the request for make payment to payment getaway.
	public function p2c2p_construct_request($parameter,$isLoggedIn) {

		if($isLoggedIn) {

			//Check stored card is enble by Merchant or not.
			if ($this->objConfigSettings['storedCard']) {
				$enable_store_card = "Y";
				$this->arrayP2c2pFormFields["enable_store_card"] = $enable_store_card;

				if(!empty($parameter['stored_card_unique_id'])) {
					$this->arrayP2c2pFormFields["stored_card_unique_id"] = $parameter['stored_card_unique_id'];
				}
			}
		}

		$this->generateP2c2pCommonFormFields($parameter);
		$this->setPaymentExpiryTime($parameter);

		$hash_value = $this->objP2c2pHashHelper->createRequestHashValue($this->arrayP2c2pFormFields,$this->objConfigSettings['secretKey']);
		$this->arrayP2c2pFormFields['hash_value']  = $hash_value;

		$strHtml = '<form name="p2c2pform" action="'. $this->getPaymentGetwayRedirectUrl() .'" method="post">';

		foreach ($this->arrayP2c2pFormFields as $key => $value) {
			if (!empty($value)) {
				$strHtml .= '<input type="hidden" name="' . htmlentities($key) . '" value="' . htmlentities($value) . '">';
			}
		}

		$strHtml .= '<input type="hidden" name="request_3ds" value="">';
		$strHtml .= '</form>';
		$strHtml .= '<script type="text/javascript">';
		$strHtml .= '	//document.p2c2pform.submit()';
		$strHtml .= '</script>';
		return $strHtml;
	}

	//This function is used to genereate the request for make payment to payment getaway.
	public function p2c2p_construct_123_request($parameter,$isLoggedIn,$debug) {

		$payload = $this->generateP123PaymentRequest($parameter);
		//$this->setPaymentExpiryTime($parameter);

		//$hash_value = $this->objP2c2pHashHelper->createRequestHashValue($this-> ,$this->objConfigSettings['secretKey']);
		//$this->arrayP2c2pFormFields['hash_value']  = $hash_value;
		if($debug){
			$strHtml = $payload;
		}else{
			$strHtml = '<form name="p123form" action="'. $this->getPaymentGetway123Url() .'" method="post">';
			$strHtml .= "<input type='hidden' name='paymentRequest' value='".$payload."'>";
			$strHtml .= '</form>';
			$strHtml .= '<script type="text/javascript">';
			$strHtml .= '	document.p123form.submit()';
			$strHtml .= '</script>';
		}
		return $strHtml;
	}

	//This function is used to genereate the request for make payment to payment getaway.
	public function p2c2p_construct_api_request($parameter,$isLoggedIn) {

		$payload = $this->generateP2c2pAPIRequest($parameter,$isLoggedIn);

		/*$pubkeyFile = dirname(__FILE__)."/keys/demo2.crt";
		$prikeyFile = dirname(__FILE__)."/keys/demo2.pem";
		$http = new HTTP();
		$_Url = $this->getPaymentGetwaySecureUrl();

		$result = $http->curlPost($_Url,'paymentRequest='.$payload);
		$pkcs7 = new pkcs7();*/
		//$response = $pkcs7->decrypt($result,$pubkeyFile,$prikeyFile,"2c2p");
		/*var_dump($response);
		echo "====================================================";*/
		//$paramRs = $this->processP2c2pResponse($response,$parameter);
		//return $paramRs;

		$strHtml = '<form name="p2c2pform" action="'. $this->getPaymentGetwaySecureUrl() .'" method="post">';
		//$strHtml = '<form name="p2c2pform" action="'. $this->getPaymentGetwayRedirectUrl() .'" method="post">';
		$strHtml .= "<input type='hidden' name='paymentRequest' value='".$payload."'>";
		$strHtml .= '</form>';
		$strHtml .= '<script type="text/javascript">';
		$strHtml .= '	document.p2c2pform.submit()';
		$strHtml .= '</script>';

		return $strHtml;
	}



	//This function is used calculate the amount by selected currency code by merchant in merchant store.
	public function getP2c2pAmountByCurrencyCode($amount) {

		$exponent = 0;
		$isFounded  = false;
		$currency_type = $this->getMerchantSelectedCurrencyCode();

		foreach ($this->objP2c2pCurrencyCodeHelper->getP2c2pSupportedCurrenyCode() as $key => $value) {
			if ($value['Num'] === $currency_type) {
				$exponent = $value['Exponent'];
				$isFounded = true;
				break;
			}
		}

		if ($isFounded) {
			if ($exponent == 0 || empty($exponent)) {
				$amount = (int) $amount;
			} else {
				$pg_2c2p_exponent = $this->objP2c2pCurrencyCodeHelper->getP2c2pSupportedCurrencyExponents();
				$multi_value      = $pg_2c2p_exponent[$exponent];
				$amount           = ($amount * $multi_value);
			}
		}

		return str_pad($amount, 12, '0', STR_PAD_LEFT);
	}

	//Creating basic request this's required by 2C2P Payment getaway.
	private function generateP2c2pAPIRequest($parameter,$isLoggedIn)
	{
		$rc_date = date('Y-m-d h:i:s');
	    $reponseTime = time();
	    $reponsefilename = '/var/www/html/2c2p/REQUEST_p2c2p_'.$reponseTime.'.txt';

		$loggerMessage = $rc_date." : genereate request to 2c2p isLoggedIn :: " . $isLoggedIn;
		file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);

		if($isLoggedIn) 
		{
			if(!empty($parameter['payment']['storeCardInfo']) && $parameter['payment']['storeCardInfo'] == 'on')
			{
				$enable_store_card = "Y";
				if(!empty($parameter['stored_card_unique_id'])) 
				{
					$stored_card_unique_id = $parameter['stored_card_unique_id'];
				}
				else
				{
					$stored_card_unique_id = "";
				}
			}
			else
			{
				$enable_store_card = "N";
				$stored_card_unique_id = "";
			}
		}

		$loggerMessage = $rc_date." : genereate request to 2c2p enable_store_card :: " . $enable_store_card;
		file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);
		$loggerMessage = $rc_date." : genereate request to 2c2p stored_card_unique_id :: " . $stored_card_unique_id;
		file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);

		/*if ($this->objConfigSettings['mode']) {
				$merchantID = "JT01";		//Get MerchantID when opening account with 2C2P
				$secretKey = "7jYcp4FxFdf0";	//Get SecretKey from 2C2P PGW Dashboard
				$currencyCode = "702";
				$panCountry = "SG";
		} else {
				
		}*/
		$secretKey = $this->objConfigSettings['secretKey'];
		$merchantID = $this->objConfigSettings['merchantId'];
		$currencyCode = $this->getMerchantSelectedCurrencyCode();

		$loggerMessage = $rc_date." : genereate request to 2c2p currencyCode :: " . $currencyCode;
		file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);

		if(!empty($parameter['payment']['cvvCardInfo']))
		{
			$loggerMessage = $rc_date." : genereate request to 2c2p cvvCardInfo :: " . $parameter['payment']['cvvCardInfo'];
		}
		else
		{
			$loggerMessage = $rc_date." : genereate request to 2c2p cvvCardInfo :: Empty";
		}

		file_put_contents($reponsefilename, $loggerMessage.PHP_EOL , FILE_APPEND | LOCK_EX);

		$panCountry=$parameter['payment']['countryCardInfo'];

		//$selected_lang = $this->objConfigSettings['toc2p_lang'];
		$selected_lang = 'TH';
		$default_lang  = !empty($selected_lang) ? $selected_lang : 'en';
		$order_id = $parameter['order_id'];
		$invoice_no = $parameter['invoice_no'];
		$cust_name = $parameter['cust_name'];
		$maskedCardNo = $parameter['payment']['maskedCardInfo'];
		$expMonth = $parameter['payment']['expMonthCardInfo'];
		$expYear = $parameter['payment']['expYearCardInfo'];
		$version="9.3";
		$timeStamp=time();
		$uniqueTransactionCode=$order_id;
		$desc=$parameter['payment_description'];
		$amt=$this->getP2c2pAmountByCurrencyCode($parameter['amount']);

		if(!empty($parameter['stored_card_unique_id'])) 
		{
			$storeCardUniqueID=$stored_card_unique_id;
			//$storeCard = "N";
		}
		//else
		//{
			$storeCard=$enable_store_card;
		//}

		//$panBank=$parameter['payment']['bankCardInfo'];
		$cardholderName=$cust_name;
		if(!empty($parameter['payment']['holderCardInfo']))
		{
			$cardholderName=$parameter['payment']['holderCardInfo'];
		}

		$cardholderEmail=$parameter['customer_email'];

		$paymentExpiry=$this->setApiPaymentExpiryTime($parameter);
		$encCardData=$parameter['encryptedCardInfo'];
		$request3DS = "Y";

		if(!empty($stored_card_unique_id)) 
		{
			$stringToHash = $version.$timeStamp.$merchantID.$uniqueTransactionCode.$desc.$amt.$currencyCode.$storeCardUniqueID.$panCountry.$cardholderName.$cardholderEmail.$storeCard.$request3DS.$paymentExpiry.$encCardData;
		}
		else
		{
			$stringToHash = $version.$timeStamp.$merchantID.$uniqueTransactionCode.$desc.$amt.$currencyCode.$panCountry.$cardholderName.$cardholderEmail.$storeCard.$request3DS.$paymentExpiry.$encCardData;
		}

		//$panBank.
		$hash = strtoupper(hash_hmac('sha1', $stringToHash ,$secretKey, false));

		$requestXml  = "<PaymentRequest>";
		$requestXml .= "	<version>$version</version>";//Mandatory
		$requestXml .= "	<timeStamp>$timeStamp</timeStamp>";//
		$requestXml .= "	<merchantID>$merchantID</merchantID>";//Mandatory
		$requestXml .= "	<uniqueTransactionCode>$uniqueTransactionCode</uniqueTransactionCode>";//Mandatory
		$requestXml .= "	<desc>$desc</desc>";//Mandatory
		$requestXml .= "	<amt>$amt</amt>";//Mandatory
		$requestXml .= "	<currencyCode>$currencyCode</currencyCode>";//Mandatory
		if(!empty($stored_card_unique_id)) {
			$requestXml .= "	<storeCardUniqueID>$storeCardUniqueID</storeCardUniqueID>";//
		}
		//$requestXml .= "	<panBank>$panBank</panBank>";//Mandatory
		$requestXml .= "	<panCountry>$panCountry</panCountry>";//Mandatory
		$requestXml .= "	<cardholderName>$cardholderName</cardholderName>";//Mandatory
		$requestXml .= "	<cardholderEmail>$cardholderEmail</cardholderEmail>";								//Mandatory
		$requestXml .= "	<storeCard>$storeCard</storeCard>";//
		$requestXml .= "	<request3DS>$request3DS</request3DS>";
		$requestXml .= "	<paymentExpiry>$paymentExpiry</paymentExpiry>";//
		$requestXml .= "	<encCardData>$encCardData</encCardData>";//Mandatory
		$requestXml .= "	<secureHash>$hash</secureHash>";;							//Mandatory
		$requestXml .= "</PaymentRequest>";

		$payload = base64_encode($requestXml);
		//$payload = urlencode($data);

 		file_put_contents($reponsefilename, $requestXml.PHP_EOL , FILE_APPEND | LOCK_EX);

		return $payload;

	}
	//Creating basic request this's required by 123 Payment getaway.
	private function generateP123PaymentRequest($parameter){

		/*if ($this->objConfigSettings['mode']) {
				$merchantID = "JT01";		//Get MerchantID when opening account with 2C2P
				$secretKey = "7jYcp4FxFdf0";	//Get SecretKey from 2C2P PGW Dashboard
				$currencyCode = "702";
				$panCountry = "SG";
		} else {
		}*/
		$secretKey = $this->objConfigSettings['secretKey'];
		$merchantID = $this->objConfigSettings['merchantId'];
		$currencyCode = $this->getMerchantSelectedCurrencyCode();

		$version="9.3";
		$selected_lang       = $this->objConfigSettings['toc2p_lang'];;
		$default_lang  = !empty($selected_lang) ? $selected_lang : 'en';
		$selected_lang = 'TH';
		$panCountry=$default_lang;
		$order_id = $parameter['order_id'];
		$invoice_no = $parameter['invoice_no'];
		$timeStamp=time();
		$uniqueTransactionCode=$order_id;
		$desc=$parameter['payment_description'];
		$amt=$this->getP2c2pAmountByCurrencyCode($parameter['amount']);
		$paymentChannel = "123";		//Set transaction as Alternative Payment Method
		$agentCode = $parameter['agentCode'];		//APM agent code
		$channelCode = $parameter['channelCode'];		//APM channel code
		$cardholderName=$parameter['cust_name'];
		$cardholderEmail=$parameter['customer_email'];
		$paymentExpiry=$this->setApiPaymentExpiryTime($parameter);
		$mobileNo=$parameter['cust_phone'];
		//$stringToHash = $version.$timeStamp.$merchantID.$uniqueTransactionCode.$desc.$amt.$currencyCode.$panBank.$panCountry.$cardholderName.$cardholderEmail.$storeCard.$request3DS.$paymentExpiry;
		$stringToHash = $version.$timeStamp.$merchantID.$uniqueTransactionCode.$desc.$amt.$currencyCode.$paymentChannel.$panCountry.$cardholderName.$cardholderEmail.$agentCode.$channelCode.$paymentExpiry.$mobileNo;

		$hash = strtoupper(hash_hmac('sha1', $stringToHash ,$secretKey, false));

		$requestXml = "<PaymentRequest>";
		$requestXml .= "<version>$version</version> ";
		$requestXml .= "<timeStamp>$timeStamp</timeStamp>";//
		$requestXml .= "<merchantID>$merchantID</merchantID>";
		$requestXml .= "<uniqueTransactionCode>$uniqueTransactionCode</uniqueTransactionCode>";
		$requestXml .= "<desc>$desc</desc>";
		$requestXml .= "<amt>$amt</amt>";
		$requestXml .= "<currencyCode>$currencyCode</currencyCode>  ";
		$requestXml .= "<panCountry>$panCountry</panCountry> ";
		$requestXml .= "<cardholderName>$cardholderName</cardholderName>";
		$requestXml .= "<paymentChannel>$paymentChannel</paymentChannel>";
		$requestXml .= "<agentCode>$agentCode</agentCode>";
		$requestXml .= "<channelCode>$channelCode</channelCode>";
		$requestXml .= "<paymentExpiry>$paymentExpiry</paymentExpiry>";
		$requestXml .= "<mobileNo>$mobileNo</mobileNo>";
		$requestXml .= "<cardholderEmail>$cardholderEmail</cardholderEmail>";
		$requestXml .= "<secureHash>$hash</secureHash>";
		$requestXml .= "</PaymentRequest>"; 
		$payload = base64_encode($requestXml);
		//$payload = urlencode($data);

		$rc_date = date('Y-m-d h:i:s');
	    $reponseTime = time();
	    $reponsefilename = '/var/www/html/2c2p/REQUEST_p123_'.$reponseTime.'.txt';
 		file_put_contents($reponsefilename, $requestXml.PHP_EOL , FILE_APPEND | LOCK_EX);
		
		return $payload;

	}
	private function processP2c2pResponse($Response,$parameter) {
		$xmlResponse=simplexml_load_string($Response);

		$currencyCode = $this->getMerchantSelectedCurrencyCode();

		$postValue = '<form name="p2c2pResponseform" action="'.urldecode($parameter['payment']['responseUrl']).'" method="post">';

		if(array_key_exists('order_id',$parameter)){	$postValue .= "<input type='hidden' name='p2c2p_id' value='".$parameter['order_id']."' />";	}
		if(array_key_exists('order_id',$parameter)){	$postValue .= "<input type='hidden' name='order_id' value='".$parameter['order_id']."' />";	}
		if(isset($xmlResponse->version)){	$postValue .= "<input type='hidden' name='version' value='".$xmlResponse->version."' />";	}
		if(isset($xmlResponse->timeStamp)){	$postValue .= "<input type='hidden' name='request_timestamp' value='".$xmlResponse->timeStamp."' />";	}
		if(isset($xmlResponse->merchantID)){	$postValue .= "<input type='hidden' name='merchant_id' value='".$xmlResponse->merchantID."' />";	}
		if(array_key_exists('invoice_no',$parameter)){	$postValue .= "<input type='hidden' name='invoice_no' value='".$parameter['invoice_no']."' />";	}
		if(isset($currencyCode)){	$postValue .= "<input type='hidden' name='currency' value='".$currencyCode."' />";	}
		if(isset($xmlResponse->amt)){	$postValue .= "<input type='hidden' name='amount' value='".$xmlResponse->amt."' />";	}
		if(isset($xmlResponse->tranRef)){	$postValue .= "<input type='hidden' name='transaction_ref' value='".$xmlResponse->tranRef."' />";	}
		if(isset($xmlResponse->approvalCode)){	$postValue .= "<input type='hidden' name='approval_code' value='".$xmlResponse->approvalCode."' />";	}
		if(isset($xmlResponse->eci)){	$postValue .= "<input type='hidden' name='eci' value='".$xmlResponse->eci."' />";	}
		if(isset($xmlResponse->dateTime)){	$postValue .= "<input type='hidden' name='transaction_datetime' value='".$xmlResponse->dateTime."' />";	}
		/*if(isset()){	$postValue .= "<input type='hidden' name='payment_channel' value='' />";	}*/
		if(isset($xmlResponse->status)){	$postValue .= "<input type='hidden' name='payment_status' value='".$xmlResponse->status."' />";	}
		if(isset($xmlResponse->respCode)){
			$postValue .= "<input type='hidden' name='channel_response_code' value='".$xmlResponse->respCode."' />";
			if(strcasecmp($xmlResponse->respCode, "000") == 0 || strcasecmp($xmlResponse->respCode, "00") == 0 ) {
				$postValue .= "<input type='hidden' name='channel_response_desc' value='success' />";
			} else if(strcasecmp($xmlResponse->respCode, "001") == 0) {
				$postValue .= "<input type='hidden' name='channel_response_desc' value='pending 2C2P' />";
			} else {
				//If payment status code is cancel/Error/other.
				$postValue .= "<input type='hidden' name='channel_response_desc' value='cancel/Error/other'/>";
			}
		}
		/*if(isset($xmlResponse->failreason)){	$postValue .= "<input type='hidden' name='channel_response_desc' value='' />";	}*/
		if(isset($xmlResponse->pan)){	$postValue .= "<input type='hidden' name='masked_pan' value='".$xmlResponse->pan."' />";	}
		if(isset($xmlResponse->storeCardUniqueID)){	$postValue .= "<input type='hidden' name='stored_card_unique_id' value='".$xmlResponse->storeCardUniqueID."' />";	}
		/*if(isset($xmlResponse->version)){	$postValue .= "<input type='hidden' name='backend_invoice' value='".."' />";	}
		if(isset($xmlResponse->version)){	$postValue .= "<input type='hidden' name='paid_channel' value='".."' />";	}
		if(isset($xmlResponse->version)){	$postValue .= "<input type='hidden' name='paid_agent' value='".."' />";	}
		if(isset($xmlResponse->version)){	$postValue .= "<input type='hidden' name='recurring_unique_id' value='".."'  />";	}*/
		if(isset($xmlResponse->userDefined1)){	$postValue .= "<input type='hidden' name='user_defined_1' value='".$xmlResponse->userDefined1."' />";	}
		if(isset($xmlResponse->userDefined2)){	$postValue .= "<input type='hidden' name='user_defined_2' value='".$xmlResponse->userDefined2."' />";	}
		if(isset($xmlResponse->userDefined3)){	$postValue .= "<input type='hidden' name='user_defined_3' value='".$xmlResponse->userDefined3."' />";	}
		if(isset($xmlResponse->userDefined4)){	$postValue .= "<input type='hidden' name='user_defined_4' value='".$xmlResponse->userDefined4."' />";	}
		if(isset($xmlResponse->userDefined5)){	$postValue .= "<input type='hidden' name='user_defined_5' value='".$xmlResponse->userDefined5."' />";	}
		if(array_key_exists('payment',$parameter)){
			if(array_key_exists('storeCardInfo',$parameter['payment'])){
				$postValue .= "<input type='hidden' name='storeCardInfo' value='".$parameter['payment']['storeCardInfo']."' />";
			}
		}
		if(isset($xmlResponse->hashValue)){	$postValue .= "<input type='hidden' name='hash_value' value='".$xmlResponse->hashValue."' />";	}
		/*if(isset()){	$postValue .= "<input type='hidden' name='browser_info' value='' />";	}
		if(isset()){	$postValue .= "<input type='hidden' name='ippPeriod' value='' />";	}
		if(isset()){	$postValue .= "<input type='hidden' name='ippInterestType' value='' />";	}
		if(isset()){	$postValue .= "<input type='hidden' name='ippInterestRate' value='' />";	}
		if(isset()){	$postValue .= "<input type='hidden' name='ippMerchantAbsorbRate' value='' />";	}*/

		$postValue .= '</form>';
		$postValue .= '<script type="text/javascript">';
		$postValue .= ' document.p2c2pResponseform.submit();';
		$postValue .= '</script>';
		return $postValue;
	}
	//Creating basic form field request this's required by 2C2P Payment getaway.
	private function generateP2c2pCommonFormFields($parameter) {

		$merchant_id      	 = $this->objConfigSettings['merchantId'];
		$currency       	 = $this->getMerchantSelectedCurrencyCode();
		$selected_lang       = $this->objConfigSettings['toc2p_lang'];;
		

		$default_lang  = !empty($selected_lang) ? $selected_lang : 'en';

		$this->arrayP2c2pFormFields["version"] 				= "7.0";
		$this->arrayP2c2pFormFields["merchant_id"] 			= $merchant_id;
		$this->arrayP2c2pFormFields["payment_description"]  = $parameter['payment_description'];
		$this->arrayP2c2pFormFields["order_id"] 			= $parameter['order_id'];
		$this->arrayP2c2pFormFields["invoice_no"] 			= $parameter['invoice_no'];
		$this->arrayP2c2pFormFields["currency"] 			= $currency;
		$this->arrayP2c2pFormFields["amount"] 				= $this->getP2c2pAmountByCurrencyCode($parameter['amount']);
		$this->arrayP2c2pFormFields["customer_email"] 		= $parameter['customer_email'];
		$this->arrayP2c2pFormFields["pay_category_id"] 		= "";
		$this->arrayP2c2pFormFields["promotion"] 			= "";
		$this->arrayP2c2pFormFields["user_defined_1"] 		= "";
		$this->arrayP2c2pFormFields["user_defined_2"] 		= "";
		$this->arrayP2c2pFormFields["user_defined_3"] 		= "";
		$this->arrayP2c2pFormFields["user_defined_4"] 		= "";
		$this->arrayP2c2pFormFields["user_defined_5"] 		= "";
		$this->arrayP2c2pFormFields["request_3ds"]    		= "";
	    $this->arrayP2c2pFormFields["result_url_1"]   		= $this->getMerchantReturnUrl();
	    $this->arrayP2c2pFormFields["result_url_2"]   		= $this->getMerchantReturnUrl();
	    $this->arrayP2c2pFormFields["payment_option"]   	= "A"; // Pass by default Payment option as A
	    $this->arrayP2c2pFormFields["default_lang"]   		= $default_lang; // Set selected language.
	}
	/*Get the selected currency code and converted this's selected currency to number instead of 3 character like 'SGD'. Because 2C2P is accept currency code in Digit only. */
    public function getMerchantSelectedCurrency() {

    	$currency_code = $this->objStoreManagerInterface->getStore()->getCurrentCurrency()->getCode();

    	foreach ($this->objP2c2pCurrencyCodeHelper->getP2c2pSupportedCurrenyCode() as $key => $value) {
    		if($key === $currency_code){
    			return  $value['Num'];
    		}
    	}

    	return "";
    }
    /*Get the selected currency code and converted this's selected currency to number instead of 3 character like 'SGD'. Because 2C2P is accept currency code in Digit only. */
    function getMerchantSelectedCurrencyCode() {

    	$currency_code = $this->objStoreManagerInterface->getStore()->getCurrentCurrency()->getCode();

    	foreach ($this->objP2c2pCurrencyCodeHelper->getP2c2pSupportedCurrenyCode() as $key => $value) {
    		if($key === $currency_code){
    			return  $value['Num'];
    		}
    	}

    	return "";
    }

	//Set the 123 payment type expiry date of the currenct time zone.
    function setApiPaymentExpiryTime($paymentBody) {

    	$payment_expiry = $this->objConfigSettings['paymentExpiry'];

    	$date           = date("Y-m-d H:i:s");
    	$strTimezone    = date_default_timezone_get();
    	$date           = new \DateTime($date, new \DateTimeZone($strTimezone));
    	$date->modify("+" . $payment_expiry . "hours");
    	$payment_expiry = $date->format("Y-m-d H:i:s");

    	return $payment_expiry;
    }


    //Set the 123 payment type expiry date of the currenct time zone.
    function setPaymentExpiryTime($paymentBody) {

    	$payment_expiry = $this->objConfigSettings['paymentExpiry'];

    	$date           = date("Y-m-d H:i:s");
    	$strTimezone    = date_default_timezone_get();
    	$date           = new \DateTime($date, new \DateTimeZone($strTimezone));
    	$date->modify("+" . $payment_expiry . "hours");
    	$payment_expiry = $date->format("Y-m-d H:i:s");

    	$this->arrayP2c2pFormFields["payment_expiry"] = $payment_expiry;
    }

    //Get Payment Getway redirect url to redirect Test URL or Live URL to 2c2p PG. It is depending upon the Merchant selected settings in configurations.
    function getPaymentGetwayRedirectUrl() {

    	if ($this->objConfigSettings['mode']) {
    		return 'https://demo2.2c2p.com/2C2PFrontEnd/RedirectV3/payment';
    	} else {
    		return 'https://t.2c2p.com/RedirectV3/payment';
    	}
    }
    //Get Payment Getway redirect url to redirect Test URL or Live URL to 2c2p PG. It is depending upon the Merchant selected settings in configurations.
    function getPaymentGetway123Url() {

    	if ($this->objConfigSettings['mode']) {
    		return 'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/PaymentAuth.aspx';
    	} else {
    		return 'https://t.2c2p.com/SecurePayment/PaymentAuth.aspx';
    	}
    }
	function getPaymentGetwaySecureUrl() {

    	if ($this->objConfigSettings['mode']) {
    		return 'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/PaymentAuth.aspx';
    	} else {
    		return 'https://t.2c2p.com/SecurePayment/PaymentAuth.aspx';
    	}
    }
    //Get the merchant website return URL.
    function getMerchantReturnUrl() {

    	$baseUrl = $this->objStoreManagerInterface->getStore()->getBaseUrl();
    	return  $baseUrl.'p2c2p/payment/response';
    }

    function get123Response($response) {
		//$paramS = $this->getRequest()->getPostValue();
		$pubkeyFile = dirname(__FILE__)."/keys/rs_key.crt";
		$prikeyFile = dirname(__FILE__)."/keys/private.pem";
		$result = $response; 
		$pkcs7 = new pkcs7();
		$responses = $pkcs7->decrypt($result,$pubkeyFile,$prikeyFile,"EyGUa6aCm4X4gN5z");
		return $responses;
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    public function processP2c2pSecureResponse($Response) {
		/*$xmlResponse=simplexml_load_string($Response);

		$currencyCode = $this->getMerchantSelectedCurrencyCode();

		$postValue = '<form name="p2c2pResponseform" action="'.urldecode($parameter['payment']['responseUrl']).'" method="post">';

		if(array_key_exists('order_id',$parameter)){	$postValue .= "<input type='hidden' name='p2c2p_id' value='".$parameter['order_id']."' />";	}
		if(array_key_exists('order_id',$parameter)){	$postValue .= "<input type='hidden' name='order_id' value='".$parameter['order_id']."' />";	}
		if(isset($xmlResponse->version)){	$postValue .= "<input type='hidden' name='version' value='".$xmlResponse->version."' />";	}
		if(isset($xmlResponse->timeStamp)){	$postValue .= "<input type='hidden' name='request_timestamp' value='".$xmlResponse->timeStamp."' />";	}
		if(isset($xmlResponse->merchantID)){	$postValue .= "<input type='hidden' name='merchant_id' value='".$xmlResponse->merchantID."' />";	}
		if(array_key_exists('invoice_no',$parameter)){	$postValue .= "<input type='hidden' name='invoice_no' value='".$parameter['invoice_no']."' />";	}
		if(isset($currencyCode)){	$postValue .= "<input type='hidden' name='currency' value='".$currencyCode."' />";	}
		if(isset($xmlResponse->amt)){	$postValue .= "<input type='hidden' name='amount' value='".$xmlResponse->amt."' />";	}
		if(isset($xmlResponse->tranRef)){	$postValue .= "<input type='hidden' name='transaction_ref' value='".$xmlResponse->tranRef."' />";	}
		if(isset($xmlResponse->approvalCode)){	$postValue .= "<input type='hidden' name='approval_code' value='".$xmlResponse->approvalCode."' />";	}
		if(isset($xmlResponse->eci)){	$postValue .= "<input type='hidden' name='eci' value='".$xmlResponse->eci."' />";	}
		if(isset($xmlResponse->dateTime)){	$postValue .= "<input type='hidden' name='transaction_datetime' value='".$xmlResponse->dateTime."' />";	}
		if(isset($xmlResponse->status)){	$postValue .= "<input type='hidden' name='payment_status' value='".$xmlResponse->status."' />";	}
		if(isset($xmlResponse->respCode)){
			$postValue .= "<input type='hidden' name='channel_response_code' value='".$xmlResponse->respCode."' />";
			if(strcasecmp($xmlResponse->respCode, "000") == 0 || strcasecmp($xmlResponse->respCode, "00") == 0 ) {
				$postValue .= "<input type='hidden' name='channel_response_desc' value='success' />";
			} else if(strcasecmp($xmlResponse->respCode, "001") == 0) {
				$postValue .= "<input type='hidden' name='channel_response_desc' value='pending 2C2P' />";
			} else {
				$postValue .= "<input type='hidden' name='channel_response_desc' value='cancel/Error/other'/>";
			}
		}
		if(isset($xmlResponse->pan)){	$postValue .= "<input type='hidden' name='masked_pan' value='".$xmlResponse->pan."' />";	}
		if(isset($xmlResponse->storeCardUniqueID)){	$postValue .= "<input type='hidden' name='stored_card_unique_id' value='".$xmlResponse->storeCardUniqueID."' />";	}
		if(isset($xmlResponse->userDefined1)){	$postValue .= "<input type='hidden' name='user_defined_1' value='".$xmlResponse->userDefined1."' />";	}
		if(isset($xmlResponse->userDefined2)){	$postValue .= "<input type='hidden' name='user_defined_2' value='".$xmlResponse->userDefined2."' />";	}
		if(isset($xmlResponse->userDefined3)){	$postValue .= "<input type='hidden' name='user_defined_3' value='".$xmlResponse->userDefined3."' />";	}
		if(isset($xmlResponse->userDefined4)){	$postValue .= "<input type='hidden' name='user_defined_4' value='".$xmlResponse->userDefined4."' />";	}
		if(isset($xmlResponse->userDefined5)){	$postValue .= "<input type='hidden' name='user_defined_5' value='".$xmlResponse->userDefined5."' />";	}
		if(array_key_exists('payment',$parameter)){
			if(array_key_exists('storeCardInfo',$parameter['payment'])){
				$postValue .= "<input type='hidden' name='storeCardInfo' value='".$parameter['payment']['storeCardInfo']."' />";
			}
		}
		if(isset($xmlResponse->hashValue)){	$postValue .= "<input type='hidden' name='hash_value' value='".$xmlResponse->hashValue."' />";	}

		$postValue .= '</form>';
		$postValue .= '<script type="text/javascript">';
		$postValue .= ' document.p2c2pResponseform.submit();';
		$postValue .= '</script>';*/

		$currencyCode = $this->getMerchantSelectedCurrencyCode();

				if(array_key_exists('uniqueTransactionCode',$Response)){	$Response['p2c2p_id'] = $Response['uniqueTransactionCode']; }
		if(array_key_exists('uniqueTransactionCode',$Response)){	$Response['order_id'] = $Response['uniqueTransactionCode']; }
		if(array_key_exists('uniqueTransactionCode',$Response)){	$Response['invoice_no'] = $Response['uniqueTransactionCode']; }
		if(array_key_exists('version',$Response)){	$Response['version'] = $Response['version']; }
		if(array_key_exists('timeStamp',$Response)){	$Response['request_timestamp'] = $Response['timeStamp']; }
		if(array_key_exists('merchantID',$Response)){	$Response['merchant_id'] = $Response['merchantID']; }

		//if(array_key_exists('order_id',$Response)){	$Response['currency'] = $this->getMerchantCurrency(); }
		$Response['currency'] = $currencyCode;
		if(array_key_exists('amt',$Response)){	$Response['amount'] = $Response['amt']; }
		if(array_key_exists('tranRef',$Response)){	$Response['transaction_ref'] = $Response['tranRef']; }
		if(array_key_exists('approvalCode',$Response)){	$Response['approval_code'] = $Response['approvalCode']; }
		if(array_key_exists('eci',$Response)){	$Response['eci'] = $Response['eci']; }
		if(array_key_exists('dateTime',$Response)){	$Response['transaction_datetime'] = $Response['dateTime']; }
		if(array_key_exists('status',$Response)){	$Response['payment_status'] = $Response['status']; }
		if(array_key_exists('userDefined1',$Response)){	$Response['user_defined_1'] = $Response['userDefined1']; }
		if(array_key_exists('userDefined2',$Response)){	$Response['user_defined_2'] = $Response['userDefined2']; }
		if(array_key_exists('userDefined3',$Response)){	$Response['user_defined_3'] = $Response['userDefined3']; }
		if(array_key_exists('userDefined4',$Response)){	$Response['user_defined_4'] = $Response['userDefined4']; }
		if(array_key_exists('userDefined5',$Response)){	$Response['user_defined_5'] = $Response['userDefined5']; }
		if(array_key_exists('pan',$Response)){	$Response['masked_pan'] = $Response['pan']; }
		if(array_key_exists('storeCardUniqueID',$Response)){ $Response['stored_card_unique_id'] = $Response['storeCardUniqueID']; }
		if(array_key_exists('respCode',$Response)){
			$Response['channel_response_code'] = $Response['respCode'];
		}
		if(array_key_exists('failReason',$Response)){
			$Response['channel_response_desc'] = $Response['failReason'];
		}

		if(array_key_exists('paidChannel',$Response)){	$Response['paid_channel'] = $Response['userDefined4']; }
		if(array_key_exists('paidAgent',$Response)){	$Response['paid_agent'] = $Response['userDefined5']; }
		//if(array_key_exists('bankName',$Response)){	$Response['user_defined_5'] = $Response['userDefined5']; }

		if(array_key_exists('hashValue',$Response)){	$Response['hash_value'] = $Response['hashValue']; }


		return $Response;
	}
}
