define(
    [
    'jquery',
    'https://t.2c2p.com/SecurePayment/api/my2c2p.1.6.9.min.js',
    //'https://demo2.2c2p.com/2C2PFrontEnd/SecurePayment/api/my2c2p.1.6.9.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/gibberish-aes/1.0.0/gibberish-aes.min.js',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/url-builder',
    'mage/storage',
    'Magento_Customer/js/customer-data',
    'Magento_Checkout/js/view/payment/default',
    'Magento_Checkout/js/action/place-order',
    'Magento_Checkout/js/action/select-payment-method',
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/checkout-data',
    'Magento_Checkout/js/model/payment/additional-validators',
    'mage/url',
    'ko',
    'Magento_Ui/js/modal/confirm'
    ],
    function (
        $,
        My2,
        GibberishAES,
        quote,
        urlBuilder,
        storage,
        customerData,
        Component,
        placeOrderAction,
        selectPaymentMethodAction,
        customer,
        checkoutData,
        additionalValidators,
        url, ko,
        confirmation
        ) {
        'use strict';

        self.specializationArray = ko.observableArray();

        if(customer.isLoggedIn()){
            $.ajax({
                type: "POST",
                url: url.build('p2c2p/token/index'),
                data: {userId : customer.customerData.id },
                async : false,
                success: function (response) {
                    self.specializationArray(response.items);
                },
                error: function (response) {
                    self.specializationArray(response);
                }
            });
        }
        return Component.extend({
            defaults: {
                template: 'P2c2p_P2c2pPayment/payment/P2c2pPayment.phtml'
            },
            tokenChangeEvent : function(){
                var token_options = $('#' +this.getCode() +'_myCardToken');
                var btnTokenId =  $('#' +this.getCode() +'_btnTokenDelete').selector;
                if(token_options.val() === ""){
                    $(btnTokenId).hide();
                    $('.tba_hide_and_seek').show();
                    $('.tba_hide_and_seek_inverse').hide();
                }else{
                    $(btnTokenId).show();
                    $('.tba_hide_and_seek').hide();
                    $('.tba_hide_and_seek_inverse').show();
                }
            },

            removeStoredCard : function(){

                var form_id = $('#' +this.getCode() +'_form').selector;
                var token_options = $('#' +this.getCode() +'_myCardToken');
                var btnTokenId =  $('#' +this.getCode() +'_btnTokenDelete').selector;

                var tokenId = token_options.val();

                if(tokenId === ""){
                    alert("Please select stored card Id.");
                    return;
                }

                if(!confirm("Are you sure you want to delete?")) return;

                $.ajax({
                    type: "POST",
                    url: url.build('p2c2p/token/remove'),
                    data: {token: tokenId},
                    async : false,
                    success: function (response) {
                        if(response === "0"){
                            alert("Unable to remove your card. Please try again, and let us know if the problem persists.");
                            return;
                        }

                        var isdeleted = $(token_options.selector +" option[value="+ tokenId + "]").remove();

                        if($(token_options.selector).find("option").length <= 1){
                            $(form_id).remove();
                        }

                        if(isdeleted.length === 0){
                            alert("Unable to remove your card. Please try again, and let us know if the problem persists.")
                        }
                        else{
                            $(btnTokenId).hide();
                            alert("Your card has been removed successfully.");
                        }
                    },
                    error: function (response) {
                        alert("Unable to remove your card. Please try again, and let us know if the problem persists.");
                        return;
                    }
                });
            },
            placeOrder: function (data, event) {

                //this.confirmPayment();
                /*if(!confirm("Are you sure you want to payment?")){
                  return false;
                }*/
                if (event) {
                    event.preventDefault();
                }
                var self = this,
                placeOrder,
                emailValidationResult = customer.isLoggedIn(),
                loginFormSelector = 'form[data-role=email-with-possible-login]';
                if (!customer.isLoggedIn()) {
                    $(loginFormSelector).validation();
                    emailValidationResult = Boolean($(loginFormSelector + ' input[name=username]').valid());
                }

                var idCode = this.getCode();
                window.GibberishAES = GibberishAES;
                var IsValidCC = true;
                window.My2c2p.getEncrypted("2c2p-payment-form",function(encryptedData,errCode,errDesc) {
                    if(errCode!=0){
                      alert(errDesc);
                      IsValidCC = false;
                    }else {
                        $('#' + idCode +'_encryptedCardInfo').val(encryptedData.encryptedCardInfo);
                        $('#' + idCode +'_maskedCardInfo').val(encryptedData.maskedCardInfo);
                        $('#' + idCode +'_expMonthCardInfo').val(encryptedData.expMonthCardInfo);
                        $('#' + idCode +'_expYearCardInfo').val(encryptedData.expYearCardInfo);
                        $('#' + idCode +'_holderCardInfo').val($('#' + idCode +'_holderCardInfos').val());
                        $('#' + idCode +'_myCardTokens').val($('#' + idCode +'_myCardToken').val());
                        $("input[value='encryptedCardInfo']").remove();
                        $("input[value='maskedCardInfo']").remove();
                        $("input[value='expMonthCardInfo']").remove();
                        $("input[value='expYearCardInfo']").remove();
                    }
                });

                if(!IsValidCC){
                    return false;
                }

                if (emailValidationResult && this.validate() && additionalValidators.validate()) {
                    this.isPlaceOrderActionAllowed(false);
                    placeOrder = placeOrderAction(this.getData(), false, this.messageContainer);

                    $.when(placeOrder).fail(function () {
                        self.isPlaceOrderActionAllowed(true);
                    }).done(this.afterPlaceOrder.bind(this));
                    return true;
                }
                return false;
            },
            getData: function() {
                return {
                    'method': this.item.method,
                    'additional_data': {
                      'myCardToken': $('#' +this.getCode() +'_myCardToken').val(),
                    }
                }
            },
            selectPaymentMethod: function () {
                selectPaymentMethodAction(this.getData());
                checkoutData.setSelectedPaymentMethod(this.item.method);
                return true;
            },
            afterPlaceOrder: function () {
                var response_url = url.build('p2c2p/payment/response');
                var idCode = this.getCode();
                $('#' + idCode +'_responseUrl').val(response_url);
                var _data = $("#2c2p-payment-form").serialize();
                //console.log(_data);
                $.ajax({
                    type: "POST",
                    url: url.build('p2c2p/payment/request'),
                    data: _data,
                    async : false,
                    success: function (response) {
                      $('#responseArea').append(response);
                    },
                    error: function (response) {
                      //console.log(response);
                      window.location.reload(true)
                    }
                });

                //window.location.replace(url.build('p2c2p/payment/request'));

            }
        });
    }
);
